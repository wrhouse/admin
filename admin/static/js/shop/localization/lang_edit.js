$("#lang_form").on('submit', (event) => {
    event.preventDefault();
    let request_data = {};
    $("form#lang_form :input[type!=submit]").each(function () {
        let input = $(this);
        request_data[input.attr('name')] = input.val();
    });
    console.log(request_data);
    $.ajax({
        url: "/api/shop/localization/language",
        method: 'PATCH',
        data: JSON.stringify(request_data),
        dataType: "application/json; charset=utf-8"
    }).done(function (response) {
        console.log(response.responseText);
    }).fail(function (response) {
        console.error(response.responseText);
    });
});