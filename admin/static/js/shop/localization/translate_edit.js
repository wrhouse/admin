// TODO: Fix submit in edit translations
$("#translate_form").on('submit', (event) => {
    event.preventDefault();
    let request_data = {
        "code": $("#text_code").val(),
        "translates": {}
    };
    $("#translates :input").each(function () {
        let input = $(this);
        if (input.val() !== "") {
            request_data["translates"][input.attr('name')] = input.val();
        } else {
            request_data["translates"][input.attr('name')] = null;
        }
    });
    console.log(request_data);
    $.ajax({
        url: "/api/shop/localization/translate",
        method: 'PATCH',
        data: JSON.stringify(request_data),
        dataType: "application/json; charset=utf-8"
    }).done(function (response) {
        console.log(response.responseText);
    }).fail(function (response) {
        console.error(response.responseText);
    });
});