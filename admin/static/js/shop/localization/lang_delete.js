$("#lang_table").on('click', '.lang_remove', function () {
    let tr = $(this).closest('tr');
    let lang_code = tr.find(".lang_code")[0].innerText;
    console.log(lang_code);
    $.ajax({
        url: "/api/shop/localization/language",
        method: 'DELETE',
        data: JSON.stringify({"lang_code": lang_code}),
        dataType: "application/json; charset=utf-8"
    }).fail(function (response) {
        console.error(response.responseText);
    }).done(function (response) {
        tr.remove();
    });
});
