$("#trans_table").on('click', '.translate_remove', function () {
    let tr = $(this).closest('tr');
    let lang_code = tr.find(".text_code")[0].innerText;
    $.ajax({
        url: "/api/shop/localization/translate",
        method: 'DELETE',
        data: JSON.stringify({"text_code": lang_code}),
        dataType: "application/json; charset=utf-8"
    }).fail(function (response) {
        console.error(response.responseText);
    }).done(function (response) {
        tr.remove();
    });
});
