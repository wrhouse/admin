const BASE_SUBMIT_URL = '/api/shop/content/delivery_option';
const REDIRECT_URL = '/shop/content/delivery_option';

function createDeliveryOption(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'POST',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function updateDeliveryOption(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'PUT',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function deleteDeliveryOption(requestData, callback) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'DELETE',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                console.log(response.responseText);
                callback();
            } else {
                console.error(response.responseText);
            }
        }
    });
}

$(document).ready(function () {
    $(document).on('click', '.delivery_option_submit', (event) => {
        event.preventDefault();
        let deliveryOptionId = $("#delivery_option_id").val();
        let requestData = {
            delivery_option_id: deliveryOptionId,
            delivery_option_name: $("#delivery_option_name").val(),
        };
        if (!deliveryOptionId) {
            createDeliveryOption(requestData)
        } else {
            updateDeliveryOption(requestData)
        }
    });

    $(document).on('click', '.delivery_option_remove', function() {
        let tr = $(this).closest('tr');
        let deliveryOptionId = tr.find(".delivery_option_id")[0].innerText;
        let requestData = {delivery_option_id: deliveryOptionId};
        deleteDeliveryOption(requestData, function() {
            tr.remove();
        });
    });
});