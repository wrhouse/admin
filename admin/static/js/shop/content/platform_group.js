const BASE_SUBMIT_URL = '/api/shop/content/platform_group';
const REDIRECT_URL = '/shop/content/platform_group';


function createPlatformGroup(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'POST',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function updatePlatformGroup(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'PUT',
        data: JSON.stringify(requestData),
        success: function (response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function deletePlatformGroup(requestData, callback) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'DELETE',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                console.log(response.responseText);
                callback();
            } else {
                console.error(response.responseText);
            }
        }
    });
}

$(document).ready(function () {
    $(document).on('click', '.platform_group_submit', (event) => {
        event.preventDefault();
        let platformGroupId = $("#platform_group_id").val();
        let requestData = {
            platform_group_id: platformGroupId,
            platform_group_name: $("#platform_group_name").val(),
        };
        if (!platformGroupId) {
            createPlatformGroup(requestData)
        } else {
            updatePlatformGroup(requestData)
        }
    });

    $(document).on('click', '.platform_group_remove', function () {
        let tr = $(this).closest('tr');
        let platformGroupId = tr.find(".platform_group_id")[0].innerText;
        let requestData = {platform_group_id: platformGroupId};
        deletePlatformGroup(requestData, function () {
            tr.remove();
        });
    });
});