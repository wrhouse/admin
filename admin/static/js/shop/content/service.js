const BASE_SUBMIT_URL = '/api/shop/content/service';
const REDIRECT_URL = '/shop/content/service';

function createService(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'POST',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function updateService(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'PUT',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function deleteService(requestData, callback) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'DELETE',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                console.log(response.responseText);
                callback();
            } else {
                console.error(response.responseText);
            }
        }
    });
}

$(document).ready(function () {
    $(document).on('click', '.service_submit', (event) => {
        event.preventDefault();
        let serviceId = $('#service_id').val();
        let requestData = {
            service_id: serviceId,
            parent_id: $('#parent_id').val(),
            service_name: $('#service_name').val(),
            service_icon: $('#service-icon-view').attr('src'),
            delivery_guidelines: $('#delivery_guidelines').val(),
            duration_days: $('#duration_days').val(),
            insurance_days: $('#insurance_days').val()
        };
        console.log(requestData);
        if (!serviceId) {
            createService(requestData);
        } else {
            updateService(requestData);
        }
    });

    $(document).on('change', '#service-icon', function() {
        let serviceIconElement = document.getElementById('service-icon');
        let serviceIconFile = new FileReader();
        serviceIconFile.readAsDataURL(serviceIconElement.files[0]);
        serviceIconFile.onloadend = function(serviceEvent) {
            let serviceIconBase64 = serviceEvent.target.result;
            let serviceView = $('#service-icon-view');
            serviceView.attr('src', serviceIconBase64);
        };
    });

    $(document).on('click', '.service_remove', function () {
        let tr = $(this).closest('tr');
        let serviceId = tr.find(".service_id")[0].innerText;
        let requestData = {service_id: serviceId};
        deleteService(requestData, function() {
            tr.remove();
        });
    });
});
