const BASE_SUBMIT_URL = '/api/shop/content/game';
const REDIRECT_URL = '/shop/content/game';

const minGameOptionValues = 2;

function createGame(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        type: 'POST',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function updateGame(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        type: 'PUT',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function deleteGame(requestData, callback) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'DELETE',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                console.log(response.responseText);
                callback();
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function collectGeneralInfo() {
    let gameId = $('#game_id').val();
    if (gameId)
        gameId = Number(gameId);
    let gamePlatformIds = $('#platform-select-multiple').val().map(Number);

    return {
        game_id: gameId,
        game_name: $('#game_name').val(),
        game_description: $('#game_description').val(),
        game_platform_ids: gamePlatformIds,
        game_image: $('#main-image-view').attr('src'),
        game_chat_image: $('#chat-image-view').attr('src')
    }
}

function collectGameOptionValues(gameOption) {
    let gameOptionValues = $($(gameOption).find('.option-values'));
    return $.map(gameOptionValues, function(gameOptionValue, gameOptionValueIdx) {
        let gameOptionValueId = $(gameOptionValue).attr('item_id');
        if (gameOptionValueId)
            gameOptionValueId = Number(gameOptionValueId);

        return {
            game_option_value_id: gameOptionValueId || null,
            game_option_value: $(gameOptionValue).val()
        };
    });
}

function collectGameOptions(service) {
    let gameOptions = $($(service).find('.game-options')[0]).children();
    return $.map(gameOptions, function(gameOption, gameOptionIdx) {
        let optionNameInput = $(gameOption).find('.option-name')[0];
        let optionPositionInput = $(gameOption).find('.option-position')[0];
        let gameOptionId = $(optionNameInput).attr('item_id');
        if (gameOptionId)
            gameOptionId = Number(gameOptionId);
        
        return {
            'game_option_id': gameOptionId || null,
            'game_option_name': $(optionNameInput).val(),
            'game_option_position': Number($(optionPositionInput).val() || 0),
            'game_option_values': collectGameOptionValues(gameOption)
        };
    });
}

function collectGameServices() {
    return $.map($('.service-box'), function(service, serviceIdx) {
        let serviceId = $($(service).find('.service-id')[0]).val();
        if (serviceId)
            serviceId = Number(serviceId);
        let deliveryOptionIds = $($(service).find('.delivery-option-select-multiple')[0]).val().map(Number);

        return {
            service_id: serviceId || null,
            price_unit_placeholder: $($(service).find('.price-unit-placeholder')[0]).val(),
            advanced_form_name: $($(service).find('.advanced-form-name')[0]).val(),
            delivery_option_ids: deliveryOptionIds,
            game_options: collectGameOptions(service)
        };
    });
}

function setGameOptionValueHandlers() {
    $(document).on('click', '.delete-game-option-value', function() {
        let buttonParentElement = $(this).parent();
        let gameValueElement = $(buttonParentElement).parent();
        gameValueElement.remove();
    });
}

function setGameOptionHandlers() {
    $(document).on('click', '.delete-game-option', function() {
        let boxFooterElement = $(this).parent();
        let boxElement = $(boxFooterElement).parent();
        boxElement.remove();
    });

    $(document).on('click', '.add-game-option-value', function() {
        let boxFooterElement = $(this).parent();
        let boxElement = $(boxFooterElement).parent();
        let gameOptionValuesElement = $(boxElement).find('.game-option-values')[0];
        $(gameOptionValuesElement).append(gameOptionValueTemplate());
    });
}

function setServiceHandlers() {
    $(document).on('click', '.delete-service', function() {
        let boxFooterElement = $(this).parent();
        let boxElement = $(boxFooterElement).parent();
        boxElement.remove();
    });

    $(document).on('click', '.add-game-option', function() {
        let boxFooterElement = $(this).parent();
        let boxElement = $(boxFooterElement).parent();
        let gameOptionValuesElement = $(boxElement).find('.game-options')[0];
        let gameOption = $(gameOptionValuesElement).append(gameOptionTemplate()).children("div:last-child");

        let addGameOptionValueButton = $(gameOption).find('.add-game-option-value');
        for (var i = 0; i < minGameOptionValues; ++i)
            $(addGameOptionValueButton).trigger('click');
    });
}

$(document).ready(function() {
    let gameBoxElement = $('#game-box');
    let gameBoxContent = gameTemplate({
        gameId: gameInfo.game_id,
        gameName: gameInfo.game_name,
        gameDescription: gameInfo.game_description,
        gamePlatformIds: gameInfo.game_platform_ids,
        gameImage: gameInfo.game_image_url,
        gameChatImage: gameInfo.game_chat_image_url
    });
    $(gameBoxElement).append(gameBoxContent);

    let platformSelectMultipleElement = $('.platform-select-multiple');
    let platformSelectValues = $('.platform-select-multiple').attr('values').split(',');
    platformSelect = platformSelectMultipleElement.select2();
    platformSelect.val(platformSelectValues).trigger('change');


    setServiceHandlers();
    setGameOptionHandlers();
    setGameOptionValueHandlers();

    let serviceBoxElement = $('#services-box');
    gameInfo.game_services.forEach(function(gameService, index, array) {
        let serviceBoxContent = serviceTemplate({
            serviceId: gameService.service_id,
            priceUnitPlaceholder: gameService.price_unit_placeholder,
            advancedFormName: gameService.advanced_form_name,
            deliveryOptionIds: gameService.delivery_option_ids
        });
        let serviceContentElement = $(serviceBoxElement).append(serviceBoxContent);

        let gameOptionsBoxElement = $(serviceContentElement).find('.game-options').last();
        gameService.game_options.forEach(function(gameOption, index, array) {
            let gameOptionContent = gameOptionTemplate({
                gameOptionId: gameOption.game_option_id,
                gameOptionName: gameOption.game_option_name,
                gameOptionPosition: gameOption.game_option_position
            });
            let gameOptionContentElement = $(gameOptionsBoxElement).append(gameOptionContent);

            let gameOptionValuesBoxElement = $(gameOptionContentElement).find('.game-option-values').last();
            gameOption.game_option_values.forEach(function(gameOptionValue, index, array) {
                let gameOptionValueContent = gameOptionValueTemplate({
                    gameOptionValueId: gameOptionValue.game_option_value_id,
                    gameOptionValue: gameOptionValue.game_option_value
                });
                $(gameOptionValuesBoxElement).append(gameOptionValueContent);
            });
        });
    });
    $('.service-id').each(function(index) {
        let thisValues = $(this).attr('values').split(',');
        let thisSelect = $(this).select2();
        thisSelect.val(thisValues).trigger('change');
    });
    $('.delivery-option-select-multiple').each(function(index) {
        let thisValues = $(this).attr('values').split(',');
        let thisSelect = $(this).select2();
        thisSelect.val(thisValues).trigger('change');
    });

    $(document).on('change', '#main-image', function() {
        let mainImageElement = document.getElementById('main-image');
        let mainImageFile = new FileReader();
        mainImageFile.readAsDataURL(mainImageElement.files[0]);
        mainImageFile.onloadend = function(mainEvent) {
            let mainImageBase64 = mainEvent.target.result;
            let main_view = $('#main-image-view');
            main_view.attr('src', mainImageBase64);
            main_view.attr('value', mainImageBase64);
        };
    });

    $(document).on('change', '#chat-image', function() {
        let chatImageElement = document.getElementById('chat-image');
        let chatImageFile = new FileReader();
        chatImageFile.readAsDataURL(chatImageElement.files[0]);
        chatImageFile.onloadend = function(chatEvent) {
            let chatImageBase64 = chatEvent.target.result;
            let chat_view = $('#chat-image-view');
            chat_view.attr('src', chatImageBase64);
            chat_view.attr('value', chatImageBase64);
        };
    });

    $(document).on('click', '.add_service', function() {
        $('#services-box').append(serviceTemplate());
        $('.delivery-option-select-multiple').select2();
    });

    $(document).on('click', '.game_submit', function() {
        let data = collectGeneralInfo();
        data.game_services = collectGameServices();

        if (data.game_id) {
            updateGame(data);
        } else {
            createGame(data);
        }
    });

    $(document).on('click', '.game_remove', function() {
        let tr = $(this).closest('tr');
        let gameId = tr.find(".game_id")[0].innerText;
        let requestData = {game_id: gameId};
        deleteGame(requestData, function() {
            tr.remove();
        });
    });
});