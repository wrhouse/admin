const BASE_SUBMIT_URL = '/api/shop/content/platform';
const REDIRECT_URL = '/shop/content/platform';


function createPlatform(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'POST',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function updatePlatform(requestData) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'PUT',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                window.location.href = REDIRECT_URL;
            } else {
                console.error(response.responseText);
            }
        }
    });
}

function deletePlatform(requestData, callback) {
    $.ajax({
        url: BASE_SUBMIT_URL,
        method: 'DELETE',
        data: JSON.stringify(requestData),
        success: function(response) {
            if (response.success) {
                console.log(response.responseText);
                callback();
            } else {
                console.error(response.responseText);
            }
        }
    });
}

$(document).ready(function () {
    $(document).on('click', '.platform_submit', (event) => {
        event.preventDefault();
        let platformId = $("#platform_id").val();
        let requestData = {
            platform_id: platformId,
            platform_name: $("#platform_name").val(),
            platform_group_id: $("#select_platform_group_single").val()
        };
        console.log(requestData);
        if (!platformId) {
            createPlatform(requestData)
        } else {
            updatePlatform(requestData)
        }
    });

    $(document).on('click', '.platform_remove', function () {
        let tr = $(this).closest('tr');
        let platformId = tr.find(".platform_id")[0].innerText;
        let requestData = {platform_id: platformId};
        deletePlatform(requestData, function () {
            tr.remove()
        });
    });
});