from sqlalchemy import Column, Integer, String

from admin.migrations import Base


class DeliveryOption(Base):
    __tablename__ = 'delivery_option'

    id = Column(Integer(), primary_key=True)
    name = Column(String(length=32), nullable=False)
