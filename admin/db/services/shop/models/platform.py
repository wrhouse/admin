from sqlalchemy import Column, Integer, ForeignKey, String

from admin.migrations import Base
from .platform_group import PlatformGroup


class Platform(Base):
    __tablename__ = 'platform'

    id = Column(Integer(), primary_key=True)
    group_id = Column(Integer(), ForeignKey(PlatformGroup.id, ondelete='CASCADE'), nullable=False)
    name = Column(String(length=32), nullable=False)
