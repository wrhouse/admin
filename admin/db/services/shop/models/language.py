from sqlalchemy import Column, String

from admin.migrations import Base


class Language(Base):
    __tablename__ = 'language'

    lang_code = Column(String(length=2), nullable=False, primary_key=True)
    lang_name = Column(String(length=32), nullable=False)
