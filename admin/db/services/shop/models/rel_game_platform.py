from sqlalchemy import Column, Integer, ForeignKey

from admin.migrations import Base
from .game import Game
from .platform import Platform


class RelGamePlatform(Base):
    __tablename__ = 'rel_game_platform'

    game_id = Column(Integer(), ForeignKey(Game.id, ondelete='CASCADE'), nullable=False, primary_key=True)
    platform_id = Column(Integer(), ForeignKey(Platform.id, ondelete='CASCADE'), nullable=False, primary_key=True)
