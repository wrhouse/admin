from sqlalchemy import Column, Integer, String, ForeignKey, Text, DateTime
from sqlalchemy.sql import literal, func

from admin.migrations import Base
from .currency import Currency
from .game import Game
from .platform import Platform
from .service import Service
from .service_duration import ServiceDuration
from .service_insurance import ServiceInsurance
from .shop import Shop


class Product(Base):
    __tablename__ = 'product'

    id = Column(Integer(), primary_key=True)
    shop_id = Column(Integer(), ForeignKey(Shop.id, ondelete='CASCADE'), nullable=False)

    name = Column(String(length=64), nullable=False)
    url = Column(String(length=255), nullable=False, index=True)
    description = Column(Text(), nullable=False, server_default='')
    thumbnail_url = Column(String(length=255), nullable=False, server_default='products/thumbnails/default.jpeg')

    currency_id = Column(Integer(), ForeignKey(Currency.id, ondelete='CASCADE'), nullable=False)
    price_per_unit = Column(Integer(), nullable=False)
    stock = Column(Integer(), nullable=False, server_default=literal(1))
    min_unit_per_order = Column(Integer(), nullable=False)

    game_id = Column(Integer(), ForeignKey(Game.id, ondelete='CASCADE'), nullable=False)
    service_id = Column(Integer(), ForeignKey(Service.id, ondelete='CASCADE'), nullable=False)
    platform_id = Column(Integer(), ForeignKey(Platform.id, ondelete='CASCADE'), nullable=False)

    insurance_id = Column(Integer(), ForeignKey(ServiceInsurance.id, ondelete='CASCADE'), nullable=True)
    duration_id = Column(Integer(), ForeignKey(ServiceDuration.id, ondelete='CASCADE'), nullable=False)

    online_hrs = Column(Integer(), nullable=False)
    offline_hrs = Column(Integer(), nullable=False)

    author_id = Column(Integer(), nullable=False)
    created_at = Column(DateTime(), server_default=func.now(), nullable=False)
