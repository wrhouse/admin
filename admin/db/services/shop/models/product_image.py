from sqlalchemy import Column, Integer, String, ForeignKey

from admin.migrations import Base
from .product import Product


class ProductImage(Base):
    __tablename__ = 'product_image'

    id = Column(Integer(), primary_key=True)
    product_id = Column(Integer(), ForeignKey(Product.id, ondelete='CASCADE'), nullable=False)
    url = Column(String(length=255), nullable=False, unique=True)
