from sqlalchemy import Column, Integer, String, DECIMAL
from sqlalchemy.sql import literal

from admin.migrations import Base


class Shop(Base):
    __tablename__ = 'shop'

    id = Column(Integer(), primary_key=True)
    owner_id = Column(Integer(), nullable=False, index=True)
    name = Column(String(length=255), nullable=False)
    url = Column(String(length=255), nullable=False, unique=True)
    rating = Column(DECIMAL(2, 1, asdecimal=False), nullable=False, server_default=literal(0.0))
    image_url = Column(String(length=255), nullable=False, server_default='stores/images/default.jpeg')
