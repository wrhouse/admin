from sqlalchemy import Column, String, Text, ForeignKey

from admin.migrations import Base
from .language import Language


class Localization(Base):
    __tablename__ = 'localization'

    text_code = Column(String(length=255), nullable=False, primary_key=True)
    lang_code = Column(String(length=2), ForeignKey(Language.lang_code, ondelete='CASCADE'), nullable=False, primary_key=True)
    text_value = Column(Text(), nullable=False)
