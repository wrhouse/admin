from functools import wraps
from typing import List

import aiohttp
import aiohttp.web


def check_group_permissions(*group_ids: List[str]):
    group_ids = set(group_ids)

    def decorator(handler):
        @wraps(handler)
        async def wrapper(request: aiohttp.web.Application):
            groups = request.get('groups', [])
            for group in groups:
                if group['group_id'] in group_ids:
                    return await handler(request)
            # TODO: Redirect to permissions denied page

        return wrapper
    return decorator
