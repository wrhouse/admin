import asyncio

import aiohttp.web
import aiohttp_debugtoolbar
import aiohttp_jinja2
import jinja2
import uvloop
from aiohttp_swagger import setup_swagger
from aiopg.sa import create_engine

from admin.jinja2_environment import setup_jinja2_filters, setup_jinja2_context_processors
from admin.middlewares import SERVICE_MIDDLEWARES
from admin.routes import init_routes
from admin.settings import Settings
from admin.utils.enums import Environment


async def database_connection(app: aiohttp.web.Application):
    settings = Settings()

    app['db'] = await create_engine(user=settings.database_user,
                                    password=settings.database_password,
                                    database=settings.database_name,
                                    host=settings.database_host,
                                    port=settings.database_port)

    yield

    app['db'].close()
    await app['db'].wait_closed()


async def shop_database_connection(app: aiohttp.web.Application):
    settings = Settings()

    app['shop_db'] = await create_engine(user=settings.shop_database_user,
                                         password=settings.shop_database_password,
                                         database=settings.shop_database_name,
                                         host=settings.shop_database_host,
                                         port=settings.shop_database_port)

    yield

    app['shop_db'].close()
    await app['shop_db'].wait_closed()


def setup_server(settings: Settings) -> aiohttp.web.Application:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    app = aiohttp.web.Application(middlewares=SERVICE_MIDDLEWARES)

    aiohttp_jinja2.setup(app=app,
                         loader=jinja2.FileSystemLoader(settings.path_to_templates),
                         filters=setup_jinja2_filters(),
                         context_processors=setup_jinja2_context_processors())

    init_routes(app=app)
    if Environment(settings.environment) != Environment.PRODUCTION:
        aiohttp_debugtoolbar.setup(app, intercept_redirects=False)
        setup_swagger(app, swagger_url='/api/reference')

    app.cleanup_ctx.extend([database_connection, shop_database_connection])

    return app


def create_app():
    settings = Settings()
    app = setup_server(settings)
    return app


if __name__ == '__main__':
    settings = Settings()

    app = setup_server(settings)
    aiohttp.web.run_app(app, host=settings.service_host, port=settings.service_port)
