import base64
from enum import Enum
import hashlib
import time
from io import BytesIO
from typing import Tuple, Optional

import aiobotocore
from PIL import Image, ImageOps

from admin.settings import Settings

settings = Settings()


class ResizeMode(Enum):
    THUMBNAIL_MODE = 1
    FIT_MODE = 2
    RESIZE_MODE = 3


async def create_and_upload_image(base64_image: str,
                                  size: Tuple[int, int] = None,
                                  directory: str = 'public',
                                  salt: Optional[int] = None,
                                  resize_mode: ResizeMode = ResizeMode.FIT_MODE,
                                  filename: Optional[str] = None) -> Optional[str]:
    if salt is None:
        salt = int(time.time())

    if base64_image.startswith('data:'):
        base64_image = base64_image.split(',')[1]

    image_bytes = BytesIO()
    image = Image.open(BytesIO(base64.b64decode(base64_image))).convert('RGB')

    if size:
        if resize_mode == ResizeMode.FIT_MODE:
            image = ImageOps.fit(image, size, Image.ANTIALIAS)
        elif resize_mode == ResizeMode.RESIZE_MODE:
            image.resize(size, Image.ANTIALIAS)
        elif resize_mode == ResizeMode.THUMBNAIL_MODE:
            image.thumbnail(size, Image.ANTIALIAS)

        image.convert('RGB').save(image_bytes, format='JPEG')
    else:
        image.save(image_bytes, format='JPEG')

    if filename is None:
        filename = f"{hashlib.md5(image_bytes.getvalue()).hexdigest()}.{salt}"
    filename = f"{filename}.jpeg"

    return await upload_to_s3(filename=filename,
                              data=image_bytes,
                              directory=directory,
                              content_type='image/jpeg')


async def upload_to_s3(filename: str,
                       data: BytesIO,
                       directory: str = 'public',
                       content_type='text/plain') -> Optional[str]:
    filepath = f'{directory}/{filename}'
    session = aiobotocore.get_session()
    async with session.create_client(
            's3', region_name='us-west-2',
            aws_secret_access_key=settings.aws_secret_access_key,
            aws_access_key_id=settings.aws_access_key_id) as client:
        response = await client.put_object(ACL='public-read',
                                           Bucket=settings.aws_bucket,
                                           Key=filepath,
                                           ContentType=content_type,
                                           Body=data.getvalue())
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return filepath


async def remove_from_s3(filename: str):
    session = aiobotocore.get_session()
    response = {}
    async with session.create_client(
            's3', region_name='us-west-2',
            aws_secret_access_key=settings.aws_secret_access_key,
            aws_access_key_id=settings.aws_access_key_id) as client:
        response = await client.delete_object(Bucket=settings.aws_bucket, Key=filename)
    return response.get('ResponseMetadata', {}).get('HTTPStatusCode', 400) == 200
