import enum


class Environment(enum.Enum):
    DEVELOPMENT = 'dev'
    SANDBOX = 'sandbox'
    PRODUCTION = 'prod'
