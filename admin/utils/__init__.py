import re

from unidecode import unidecode


__WHITESPACE_PATTERN__ = re.compile('\s+')
__ANOTHER_SYMBOLS__ = re.compile('[^\s\w\d]+')


def url_encode(text: str) -> str:
    translited_text = unidecode(text).lower().strip()
    text_without_another_symbols = __ANOTHER_SYMBOLS__.sub('', translited_text)
    text_without_whitespaces = __WHITESPACE_PATTERN__.sub('_', text_without_another_symbols)
    return text_without_whitespaces
