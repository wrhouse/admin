from typing import List, Tuple

import sqlalchemy as sa
from aiopg.sa import Engine

from admin.db.services.shop.models import Language, Localization


async def get_languages(engine: Engine) -> List[Language]:
    """
    Get all available languages
    """
    select_query = sa.select([
        Language.lang_code,
        Language.lang_name
    ])
    async with engine.acquire() as conn:
        cursor = await conn.execute(select_query)
        languages = await cursor.fetchall()
    return languages


async def get_language_by_code(engine: Engine, lang_code: str) -> Language:
    select_query = sa.select([Language], Language.lang_code == lang_code)
    async with engine.acquire() as conn:
        cursor = await conn.execute(select_query)
        res = await cursor.fetchone()
    return res


async def update_language_by_code(engine: Engine, code: str, updated_values: dict) -> None:
    update_query = sa.update(Language, Language.lang_code == code, updated_values)
    async with engine.acquire() as conn:
        await conn.execute(update_query)


async def get_translates(engine: Engine) -> List[Localization]:
    select_query = sa.select([Localization.text_code]).group_by(Localization.text_code)
    async with engine.acquire() as conn:
        cursor = await conn.execute(select_query)
        res = await cursor.fetchall()
    return res


async def get_translates_by_code(engine: Engine, text_code) -> List[Localization]:
    select_query = sa.select([Localization]).where(
        Localization.text_code == text_code)
    async with engine.acquire() as conn:
        cursor = await conn.execute(select_query)
        res = await cursor.fetchall()
    return res
