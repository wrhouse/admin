import random
import socket
import time

from admin.settings import Settings

if __name__ == "__main__":
    settings = Settings()
    postgres_connected = "FAILED"
    shop_postgres_connected = "FAILED"
    while True:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                sock.connect((settings.database_host, settings.database_port))
                postgres_connected = "  OK  "
                print("[  OK  ] Postgresql connected")

            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                sock.connect((settings.shop_database_host, settings.shop_database_port))
                shop_postgres_connected = "  OK  "
                print("[  OK  ] Shop Postgresql connected")

            break
        except socket.error:
            print(f"[{postgres_connected}] Postgresql connected")
            print(f"[{shop_postgres_connected}] Shop Postgresql connected")
            time.sleep(0.5 + (random.randint(0, 100) / 1000))
