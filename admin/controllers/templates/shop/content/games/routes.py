import aiohttp.web

from admin.controllers.templates.shop.content.games.views.create import create
from admin.controllers.templates.shop.content.games.views.get_list import get_list
from admin.controllers.templates.shop.content.games.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='content.shop.games.manage')
    app.router.add_get(prefix + '/create', create, name='content.shop.games.create')
    app.router.add_get(prefix + r'/{game_id:\d+}', update, name='content.shop.games.update')
