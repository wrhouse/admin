import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.delivery_option import fn_get_delivery_option
from admin.crud.shop.content.platform import fn_get_platform
from admin.crud.shop.content.service import fn_get_service


@aiohttp_jinja2.template('pages/shop/content/games/edit.jinja2')
async def create(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as connection:
        platforms = await fn_get_platform(connection)
        services = await fn_get_service(connection, with_parent_prefix=True)
        delivery_options = await fn_get_delivery_option(connection)

    return {
        'platforms': platforms,
        'services': services,
        'delivery_options': delivery_options
    }
