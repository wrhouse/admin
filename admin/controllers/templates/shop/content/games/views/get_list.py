import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.game import fn_get_game
from admin.crud.shop.content.platform import fn_get_platform
from admin.crud.shop.content.service import fn_get_service


@aiohttp_jinja2.template('pages/shop/content/games/manage.jinja2')
async def get_list(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as connection:
        extended_platforms = await fn_get_platform(connection)
        extended_services = await fn_get_service(connection, with_parent_prefix=True)
        games = await fn_get_game(connection, extended=True)

    platform_name_by_id = {extended_platform.platform.id: extended_platform.platform.name
                           for extended_platform in extended_platforms}
    service_name_by_id = {extended_service.service.id: extended_service.full_name
                          for extended_service in extended_services}

    return {
        'games': games,
        'platform_name_by_id': platform_name_by_id,
        'service_name_by_id': service_name_by_id
    }
