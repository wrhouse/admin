import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.delivery_option import fn_get_delivery_option
from admin.crud.shop.content.game import fn_get_game
from admin.crud.shop.content.platform import fn_get_platform
from admin.crud.shop.content.service import fn_get_service


@aiohttp_jinja2.template('pages/shop/content/games/edit.jinja2')
async def update(request: aiohttp.web.Request):
    game_id = int(request.match_info['game_id'])
    async with request.app['shop_db'].acquire() as connection:
        game = (await fn_get_game(connection, game_id=game_id, extended=True))[0]
        platforms = await fn_get_platform(connection)
        services = await fn_get_service(connection, with_parent_prefix=True)
        delivery_options = await fn_get_delivery_option(connection)

    return {
        'game': game,
        'platforms': platforms,
        'services': services,
        'delivery_options': delivery_options
    }
