import aiohttp.web

from admin.controllers.templates.shop.content.platform_groups.views.create import create
from admin.controllers.templates.shop.content.platform_groups.views.get_list import get_list
from admin.controllers.templates.shop.content.platform_groups.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='content.shop.platform_groups.manage')
    app.router.add_get(prefix + '/create', create, name='content.shop.platform_groups.create')
    app.router.add_get(prefix + r'/{platform_group_id:\d+}', update, name='content.shop.platform_groups.update')
