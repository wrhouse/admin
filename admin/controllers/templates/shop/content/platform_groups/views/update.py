import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.platform_group import fn_get_platform_group


@aiohttp_jinja2.template('pages/shop/content/platform_groups/edit.jinja2')
async def update(request: aiohttp.web.Request):
    group_platform_id = int(request.match_info['platform_group_id'])
    async with request.app['shop_db'].acquire() as conn:
        group_platform = (await fn_get_platform_group(conn,
                                                      platform_group_id=group_platform_id))[0]

    return {
        'title': f'Редактирование группы платформ "{group_platform.name}"',
        'group_platform': group_platform,
    }
