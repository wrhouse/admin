import aiohttp.web
import aiohttp_jinja2
from sqlalchemy import select

from admin.db.services.shop.models import PlatformGroup


@aiohttp_jinja2.template('pages/shop/content/platform_groups/manage.jinja2')
async def get_list(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as conn:
        select_query = select([PlatformGroup], use_labels=True)
        cursor = await conn.execute(select_query)
        platform_groups = await cursor.fetchall()

    return {
        'platform_groups': platform_groups
    }
