import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/shop/content/platform_groups/edit.jinja2')
async def create(request: aiohttp.web.Request):
    return {
        'title': f'Новая группа платформ'
    }
