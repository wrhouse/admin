import aiohttp.web

from admin.controllers.templates.shop.content.products.views.create import create
from admin.controllers.templates.shop.content.products.views.get_list import get_list
from admin.controllers.templates.shop.content.products.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='content.shop.products.manage')
    app.router.add_get(prefix + '/create', create, name='content.shop.products.create')
    app.router.add_get(prefix + r'/{product_id:\d+}', update)
