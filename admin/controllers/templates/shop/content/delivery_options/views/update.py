import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.delivery_option import fn_get_delivery_option


@aiohttp_jinja2.template('pages/shop/content/delivery_options/edit.jinja2')
async def update(request: aiohttp.web.Request):
    delivery_option_id = int(request.match_info['delivery_option_id'])
    async with request.app['shop_db'].acquire() as conn:
        delivery_option = (await fn_get_delivery_option(conn,
                                                        delivery_option_id=delivery_option_id))[0]

    return {
        'title': f'Редактирование опции доставки "{delivery_option.name}"',
        'delivery_option': delivery_option,
    }
