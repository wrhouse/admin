import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.delivery_option import fn_get_delivery_option


@aiohttp_jinja2.template('pages/shop/content/delivery_options/manage.jinja2')
async def get_list(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as conn:
        delivery_options = await fn_get_delivery_option(conn)

    return {
        'delivery_options': delivery_options
    }
