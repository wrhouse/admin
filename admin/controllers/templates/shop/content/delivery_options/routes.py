import aiohttp.web

from admin.controllers.templates.shop.content.delivery_options.views.create import create
from admin.controllers.templates.shop.content.delivery_options.views.get_list import get_list
from admin.controllers.templates.shop.content.delivery_options.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='content.shop.delivery_options.manage')
    app.router.add_get(prefix + '/create', create, name='content.shop.delivery_options.create')
    app.router.add_get(prefix + r'/{delivery_option_id:\d+}', update, name='content.shop.delivery_options.update')
