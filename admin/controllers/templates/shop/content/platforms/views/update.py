import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.platform import fn_get_platform
from admin.crud.shop.content.platform_group import fn_get_platform_group


@aiohttp_jinja2.template('pages/shop/content/platforms/edit.jinja2')
async def update(request: aiohttp.web.Request):
    platform_id = int(request.match_info['platform_id'])
    async with request.app['shop_db'].acquire() as conn:
        extended_platform = (await fn_get_platform(conn,
                                                   platform_id=platform_id))[0]
        all_platform_groups = await fn_get_platform_group(conn)

    return {
        'title': f'Редактирование платформы "{extended_platform.platform.name}"',
        'platform': extended_platform.platform,
        'platform_group': extended_platform.platform_group,
        'all_platform_groups': all_platform_groups
    }
