import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.platform_group import fn_get_platform_group


@aiohttp_jinja2.template('pages/shop/content/platforms/edit.jinja2')
async def create(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as conn:
        all_platform_groups = await fn_get_platform_group(conn)

    return {
        'title': f'Новая платформа',
        'all_platform_groups': all_platform_groups
    }
