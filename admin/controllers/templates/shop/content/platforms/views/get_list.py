import aiohttp.web
import aiohttp_jinja2
from sqlalchemy import select, join

from admin.db.services.shop.models import Platform, PlatformGroup


@aiohttp_jinja2.template('pages/shop/content/platforms/manage.jinja2')
async def get_list(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as conn:
        select_query = (select([Platform.id, Platform.name, PlatformGroup.name], use_labels=True)
                        .select_from(join(Platform,
                                          PlatformGroup,
                                          Platform.group_id == PlatformGroup.id)))
        cursor = await conn.execute(select_query)
        platforms_with_groups = await cursor.fetchall()

    return {
        'platforms_with_groups': platforms_with_groups
    }
