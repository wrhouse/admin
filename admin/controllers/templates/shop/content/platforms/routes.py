import aiohttp.web

from admin.controllers.templates.shop.content.platforms.views.create import create
from admin.controllers.templates.shop.content.platforms.views.get_list import get_list
from admin.controllers.templates.shop.content.platforms.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='content.shop.platforms.manage')
    app.router.add_get(prefix + '/create', create, name='content.shop.platforms.create')
    app.router.add_get(prefix + r'/{platform_id:\d+}', update, name='content.shop.platforms.update')
