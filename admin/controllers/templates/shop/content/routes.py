import aiohttp.web

from admin.controllers.templates.shop.content.delivery_options import routes as delivery_options_routes
from admin.controllers.templates.shop.content.games import routes as games_routes
from admin.controllers.templates.shop.content.platform_groups import routes as platform_groups_routes
from admin.controllers.templates.shop.content.platforms import routes as platforms_routes
from admin.controllers.templates.shop.content.products import routes as products_routes
from admin.controllers.templates.shop.content.services import routes as services_routes
from admin.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    include_urls(app, prefix + '/delivery_option', delivery_options_routes)
    include_urls(app, prefix + '/game', games_routes)
    include_urls(app, prefix + '/platform_group', platform_groups_routes)
    include_urls(app, prefix + '/platform', platforms_routes)
    include_urls(app, prefix + '/product', products_routes)
    include_urls(app, prefix + '/service', services_routes)
