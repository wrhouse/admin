import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/shop/content/delivery_options/manage.jinja2')
async def delivery_options_list(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/delivery_options/update.jinja2')
async def create_delivery_option(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/delivery_options/update.jinja2')
async def update_delivery_option(request: aiohttp.web.Request):
    return {}
