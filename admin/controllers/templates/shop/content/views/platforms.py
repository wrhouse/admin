import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/shop/content/platforms/manage.jinja2')
async def platforms_list(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/platforms/update.jinja2')
async def create_platform(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/platforms/update.jinja2')
async def update_platform(request: aiohttp.web.Request):
    return {}
