import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/shop/content/games/manage.jinja2')
async def games_list(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/games/update.jinja2')
async def create_game(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/games/update.jinja2')
async def update_game(request: aiohttp.web.Request):
    return {}
