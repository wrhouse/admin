import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/shop/content/services/manage.jinja2')
async def services_list(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/services/update.jinja2')
async def create_service(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/services/update.jinja2')
async def update_service(request: aiohttp.web.Request):
    return {}
