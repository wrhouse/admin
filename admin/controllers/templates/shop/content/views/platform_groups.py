import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/shop/content/platform_groups/manage.jinja2')
async def platform_groups_list(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/platform_groups/update.jinja2')
async def create_platform_group(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/platform_groups/update.jinja2')
async def update_platform_group(request: aiohttp.web.Request):
    return {}
