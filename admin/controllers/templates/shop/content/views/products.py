import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/shop/content/products/manage.jinja2')
async def products_list(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/products/update.jinja2')
async def create_product(request: aiohttp.web.Request):
    return {}


@aiohttp_jinja2.template('pages/shop/content/products/update.jinja2')
async def update_product(request: aiohttp.web.Request):
    return {}
