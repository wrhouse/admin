import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.service import fn_get_service


@aiohttp_jinja2.template('pages/shop/content/services/edit.jinja2')
async def create(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as connection:
        parent_services = [extended_service for extended_service in await fn_get_service(connection, with_parent_prefix=True)]

    return {
        'title': 'Новая услуга',
        'parent_services': parent_services
    }
