import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.service import fn_get_service


@aiohttp_jinja2.template('pages/shop/content/services/edit.jinja2')
async def update(request: aiohttp.web.Request):
    service_id = int(request.match_info['service_id'])
    async with request.app['shop_db'].acquire() as connection:
        extended_service = (await fn_get_service(connection,
                                                 service_id=service_id,
                                                 extended=True))[0]
        parent_services = [extended_service
                           for extended_service in await fn_get_service(connection)
                           if extended_service.service.id != service_id]

    return dict(
        title=f'Редактирование услуги "{extended_service.service.name}"',
        service=extended_service.service,
        insurance_days=extended_service.insurance_days,
        duration_days=extended_service.duration_days,
        parent_services=parent_services
    )
