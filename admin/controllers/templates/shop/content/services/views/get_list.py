import aiohttp.web
import aiohttp_jinja2

from admin.crud.shop.content.service import fn_get_service


@aiohttp_jinja2.template('pages/shop/content/services/manage.jinja2')
async def get_list(request: aiohttp.web.Request):
    async with request.app['shop_db'].acquire() as conn:
        extended_services = await fn_get_service(conn, extended=True, with_parent_prefix=True)

    return {
        'extended_services': extended_services
    }
