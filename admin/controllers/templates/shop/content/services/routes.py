import aiohttp.web

from admin.controllers.templates.shop.content.services.views.create import create
from admin.controllers.templates.shop.content.services.views.get_list import get_list
from admin.controllers.templates.shop.content.services.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='content.shop.services.manage')
    app.router.add_get(prefix + '/create', create, name='content.shop.services.create')
    app.router.add_get(prefix + r'/{service_id:\d+}', update, name='content.shop.services.update')
