import aiohttp.web

from admin.controllers.templates.shop.localization.languages.views.create import create
from admin.controllers.templates.shop.localization.languages.views.get_list import get_list
from admin.controllers.templates.shop.localization.languages.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='localization.shop.language.manage')
    app.router.add_get(prefix + '/create', create, name='localization.shop.language.create')
    app.router.add_get(prefix + r'/{lang_code}', update, name='localization.shop.language.update')
