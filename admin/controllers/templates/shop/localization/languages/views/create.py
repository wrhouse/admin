import aiohttp_jinja2
from aiohttp import web


@aiohttp_jinja2.template('pages/shop/localization/languages/create.jinja2')
async def create(request: web.Request):
    return {}
