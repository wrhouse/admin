import aiohttp.web
import aiohttp_jinja2

from admin.utils.localization import get_languages


@aiohttp_jinja2.template('pages/shop/localization/languages/manage.jinja2')
async def get_list(request: aiohttp.web.Request):
    languages = await get_languages(request.app['shop_db'])
    return {"languages": languages}
