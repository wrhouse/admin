import aiohttp_jinja2
from aiohttp import web

from admin.utils.localization import get_language_by_code


@aiohttp_jinja2.template('pages/shop/localization/languages/edit.jinja2')
async def update(request: web.Request):
    engine = request.app['shop_db']
    lang_code = request.match_info['lang_code']
    lang_info = await get_language_by_code(engine, lang_code)
    return {'lang_info': lang_info}
