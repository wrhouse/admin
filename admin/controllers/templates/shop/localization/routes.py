import aiohttp.web

from admin.controllers.templates.shop.localization.languages import routes as languages_routes
from admin.controllers.templates.shop.localization.translates import routes as translate_routes
from admin.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    include_urls(app, prefix + '/language', languages_routes)
    include_urls(app, prefix + "/translates", translate_routes)
