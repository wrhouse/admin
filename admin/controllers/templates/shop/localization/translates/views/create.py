import aiohttp_jinja2
from aiohttp import web

from admin.utils.localization import get_languages


@aiohttp_jinja2.template('pages/shop/localization/translates/create.jinja2')
async def create(request: web.Request):
    engine = request.app['shop_db']
    languages = await get_languages(engine)
    return {"languages": languages}
