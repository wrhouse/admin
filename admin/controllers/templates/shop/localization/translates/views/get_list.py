import aiohttp.web
import aiohttp_jinja2

from admin.utils.localization import get_translates


@aiohttp_jinja2.template('pages/shop/localization/translates/manage.jinja2')
async def get_list(request: aiohttp.web.Request):
    translates = await get_translates(request.app['shop_db'])
    translates = [x.text_code for x in translates]
    return {"translates": translates}
