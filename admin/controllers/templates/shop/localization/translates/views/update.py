import aiohttp_jinja2
from aiohttp import web

from admin.utils.localization import get_languages, get_translates_by_code


@aiohttp_jinja2.template('pages/shop/localization/translates/edit.jinja2')
async def update(request: web.Request):
    engine = request.app['shop_db']
    text_code = request.match_info['text_code']
    languages = await get_languages(engine)
    translates = await get_translates_by_code(engine, text_code)
    res_translates = {x.lang_code: "" for x in languages}
    for trans in translates:
        res_translates[trans.lang_code] = trans.text_value

    return {"languages": languages, "text_code": text_code, "translates": res_translates}
