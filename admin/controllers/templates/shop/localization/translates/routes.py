import aiohttp.web

from admin.controllers.templates.shop.localization.translates.views.create import create
from admin.controllers.templates.shop.localization.translates.views.get_list import get_list
from admin.controllers.templates.shop.localization.translates.views.update import update


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_get(prefix, get_list, name='localization.shop.translate.manage')
    app.router.add_get(prefix + '/create', create, name='localization.shop.translate.create')
    app.router.add_get(prefix + r'/{text_code}', update, name='localization.shop.translate.update')
