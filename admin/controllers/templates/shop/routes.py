import aiohttp.web

from admin.controllers.templates.shop.content import routes as content_routes
from admin.controllers.templates.shop.localization import routes as localization_routes
from admin.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    include_urls(app, prefix + '/content', content_routes)
    include_urls(app, prefix + '/localization', localization_routes)
