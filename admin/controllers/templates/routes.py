import aiohttp.web

from admin.controllers.templates.shop import routes as shop_routes
from admin.controllers.templates.views.index import index
from admin.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    include_urls(app, '/shop', shop_routes)

    app.router.add_get('/', index, name='index')
