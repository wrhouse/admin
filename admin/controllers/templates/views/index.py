import aiohttp.web
import aiohttp_jinja2


@aiohttp_jinja2.template('pages/index.jinja2')
async def index(request: aiohttp.web.Request):
    return {}
