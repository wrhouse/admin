import aiohttp

import admin.controllers.api.shop.localization.views.language as lang
import admin.controllers.api.shop.localization.views.translates as trans


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    app.router.add_patch(prefix + "/language", lang.update)
    app.router.add_put(prefix + "/language", lang.create)
    app.router.add_delete(prefix + "/language", lang.remove)
    app.router.add_put(prefix + "/translate", trans.create)
    app.router.add_patch(prefix + "/translate", trans.update)
    app.router.add_delete(prefix + "/translate", trans.delete)
