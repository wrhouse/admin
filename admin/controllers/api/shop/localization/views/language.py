import aiohttp

from admin.crud.shop.localization.language import fn_update_language, fn_add_language, fn_delete_language
from admin.utils.responses import error_response, success_response


async def update(request: aiohttp.web.Request):
    engine = request.app['shop_db']
    form_data = await request.json()
    try:
        async with engine.acquire() as conn:
            await fn_update_language(conn, **form_data)
        return success_response("Updated")
    except Exception as e:
        return error_response(str(e))


async def create(request: aiohttp.web.Request):
    engine = request.app['shop_db']
    form_data = await request.json()
    if form_data.get("lang_code").strip() == "" or form_data.get("lang_name").strip() == "":
        return error_response("Empty data")
    try:
        async with engine.acquire() as conn:
            await fn_add_language(conn, **form_data)
        return success_response("Created")
    except Exception as e:
        return error_response(str(e))


async def remove(request: aiohttp.web.Request):
    engine = request.app['shop_db']
    form_data = await request.json()
    lang_code = form_data.get('lang_code')
    try:
        async with engine.acquire() as conn:
            await fn_delete_language(conn, lang_code=lang_code)
        return success_response("Removed")
    except Exception as e:
        return error_response(str(e))
