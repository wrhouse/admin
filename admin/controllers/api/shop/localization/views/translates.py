import aiohttp

from admin.crud.shop.localization.localization import fn_add_localization_text, fn_delete_localization_text, \
    fn_create_or_update_localization_text, fn_delete_localization
from admin.utils.responses import error_response, success_response


async def update(request: aiohttp.web.Request):
    engine = request.app['shop_db']
    form_data = await request.json()
    text_code = form_data.pop('code')
    try:
        for language, text in form_data.pop('translates').items():
            async with engine.acquire() as conn:
                if text is not None:
                    await fn_create_or_update_localization_text(conn,
                                                                lang_code=language,
                                                                text_code=text_code,
                                                                text_value=text)
                else:
                    await fn_delete_localization_text(conn, lang_code=language, text_code=text_code)
        return success_response("Updated")
    except Exception as e:
        return error_response(str(e))


async def create(request: aiohttp.web.Request):
    engine = request.app['shop_db']
    form_data = await request.json()
    text_code = form_data.pop('code')
    if text_code.strip() == "":
        return error_response("Empty code")
    try:
        for language, text in form_data.pop('translates').items():
            async with engine.acquire() as conn:
                await fn_add_localization_text(conn, lang_code=language, text_code=text_code, text_value=text)
        return success_response("Created")
    except Exception as e:
        return error_response(str(e))


async def delete(request: aiohttp.web.Request):
    engine = request.app['shop_db']
    form_data = await request.json()
    text_code = form_data.get('text_code')
    try:
        async with engine.acquire() as conn:
            await fn_delete_localization(conn, text_code=text_code)
        return success_response("Removed")
    except Exception as e:
        return error_response(str(e))
