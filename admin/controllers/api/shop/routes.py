import aiohttp

import admin.controllers.api.shop.localization.routes as lang_routes
import admin.controllers.api.shop.content.routes as content_routes
from admin.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    include_urls(app, prefix + "/localization", lang_routes)
    include_urls(app, prefix + "/content", content_routes)
