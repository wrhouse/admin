import aiohttp.web

from admin.crud.shop.content.platform_group import fn_create_platform_group, fn_update_platform_group, \
    fn_delete_platform_group
from admin.utils.responses import success_response


async def create_platform_group(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_create_platform_group(conn, platform_group_name=body['platform_group_name'])
        return success_response("Created")


async def update_platform_group(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_update_platform_group(conn, platform_group_id=body['platform_group_id'],
                                       platform_group_name=body['platform_group_name'])
        return success_response("Updated")


async def delete_platform_group_by_id(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_delete_platform_group(conn, platform_group_id=body['platform_group_id'])
        return success_response("Deleted")
