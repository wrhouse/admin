import aiohttp.web

from admin.crud.shop.content.delivery_option import fn_create_delivery_option, fn_update_delivery_option, \
    fn_delete_delivery_option
from admin.utils.responses import success_response


async def create_delivery_option(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_create_delivery_option(conn, delivery_option_name=body['delivery_option_name'])
        return success_response("Created")


async def update_delivery_option(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_update_delivery_option(conn,
                                        delivery_option_id=body['delivery_option_id'],
                                        delivery_option_name=body['delivery_option_name'])
        return success_response("Updated")


async def delete_delivery_option_by_id(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_delete_delivery_option(conn,
                                        delivery_option_id=body['delivery_option_id'])
        return success_response("Deleted")
