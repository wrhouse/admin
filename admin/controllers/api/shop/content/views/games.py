import aiohttp.web

from admin.crud.shop.content.game import fn_delete_game, fn_create_game, fn_update_game

from admin.utils.responses import success_response


async def create_game(request: aiohttp.web.Request):
    game_json = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as connection:
        await fn_create_game(db_connection=connection,
                             game_name=game_json['game_name'],
                             game_description=game_json['game_description'],
                             game_image=game_json.get('game_image') or None,
                             game_chat_image=game_json.get('game_chat_image') or None,
                             game_platform_ids=game_json['game_platform_ids'],
                             game_services=game_json['game_services'])

    return success_response("Created")


async def update_game(request: aiohttp.web.Request):
    game_json = await request.json()
    engine = request.app['shop_db']

    async with engine.acquire() as connection:
        await fn_update_game(db_connection=connection,
                             game_id=game_json['game_id'],
                             game_name=game_json['game_name'],
                             game_description=game_json['game_description'],
                             game_image=game_json.get('game_image') or None,
                             game_chat_image=game_json.get('game_chat_image') or None,
                             game_platform_ids=game_json['game_platform_ids'],
                             game_services=game_json['game_services'])

    return success_response("Updated")


async def delete_game_by_id(request: aiohttp.web.Request):
    engine = request.app['shop_db']
    body = await request.json()
    async with engine.acquire() as conn:
        await fn_delete_game(conn, game_id=body['game_id'])
        return success_response('Removed')
