import aiohttp.web

from admin.crud.shop.content.platform import fn_create_platform, fn_update_platform, fn_delete_platform
from admin.utils.responses import success_response


async def create_platform(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_create_platform(conn,
                                 platform_name=body['platform_name'],
                                 platform_group_id=body['platform_group_id'])
        return success_response("Created")


async def update_platform(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_update_platform(conn,
                                 platform_id=body['platform_id'],
                                 platform_name=body['platform_name'],
                                 platform_group_id=body['platform_group_id'])
        return success_response("Updated")


async def delete_platform_by_id(request: aiohttp.web.Request):
    body = await request.json()
    engine = request.app['shop_db']
    async with engine.acquire() as conn:
        await fn_delete_platform(conn,
                                 platform_id=body['platform_id'])
        return success_response("Removed")
