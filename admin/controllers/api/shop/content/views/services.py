from typing import List

from aiohttp import web

from admin.crud.shop.content.service import fn_create_service, fn_update_service, fn_delete_service
from admin.utils.responses import success_response


def str_to_int_list(source: str) -> List[int]:
    if source.strip():
        return list(map(int, source.split(',')))
    return []


async def update_service(request: web.Request):
    service_json = await request.json()
    engine = request.app['shop_db']

    duration_days = str_to_int_list(service_json['duration_days'])
    insurance_days = str_to_int_list(service_json['insurance_days'])

    parent_id = service_json['parent_id'] or None
    if parent_id is not None:
        parent_id = int(parent_id)

    async with engine.acquire() as conn:
        await fn_update_service(
            conn,
            service_id=int(service_json['service_id']),
            parent_id=parent_id,
            service_name=service_json['service_name'],
            icon=service_json.get('service_icon'),
            delivery_guidelines=service_json['delivery_guidelines'],
            duration_days=duration_days,
            insurance_days=insurance_days
        )
    return success_response("Updated")


async def create_service(request: web.Request):
    service_json = await request.json()
    engine = request.app['shop_db']

    duration_days = str_to_int_list(service_json['duration_days'])
    insurance_days = str_to_int_list(service_json['insurance_days'])

    parent_id = service_json['parent_id'] or None
    if parent_id is not None:
        parent_id = int(parent_id)

    async with engine.acquire() as conn:
        await fn_create_service(
            conn,
            service_name=service_json['service_name'],
            parent_id=parent_id,
            icon=service_json.get('service_icon'),
            delivery_guidelines=service_json['delivery_guidelines'],
            duration_days=duration_days,
            insurance_days=insurance_days
        )
    return success_response("Created")


async def delete_service_by_id(request: web.Request):
    engine = request.app['shop_db']
    body = await request.json()
    async with engine.acquire() as conn:
        await fn_delete_service(conn, service_id=body['service_id'])
        return success_response('Removed')
