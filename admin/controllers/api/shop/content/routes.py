from aiohttp import web

from admin.controllers.api.shop.content.views.delivery_options import (
    create_delivery_option,
    update_delivery_option,
    delete_delivery_option_by_id
)
from admin.controllers.api.shop.content.views.games import (
    create_game,
    update_game,
    delete_game_by_id
)
from admin.controllers.api.shop.content.views.platform_groups import (
    create_platform_group,
    update_platform_group,
    delete_platform_group_by_id
)
from admin.controllers.api.shop.content.views.platforms import (
    create_platform,
    update_platform,
    delete_platform_by_id
)
from admin.controllers.api.shop.content.views.services import (
    delete_service_by_id,
    create_service,
    update_service
)


def init_routes(app: web.Application,
                prefix: str) -> None:
    app.router.add_post(prefix + "/delivery_option", create_delivery_option)
    app.router.add_put(prefix + "/delivery_option", update_delivery_option)
    app.router.add_delete(prefix + "/delivery_option", delete_delivery_option_by_id)

    app.router.add_post(prefix + "/game", create_game)
    app.router.add_put(prefix + "/game", update_game)
    app.router.add_delete(prefix + "/game", delete_game_by_id)

    app.router.add_post(prefix + "/service", create_service)
    app.router.add_put(prefix + "/service", update_service)
    app.router.add_delete(prefix + "/service", delete_service_by_id)

    app.router.add_post(prefix + "/platform_group", create_platform_group)
    app.router.add_put(prefix + "/platform_group", update_platform_group)
    app.router.add_delete(prefix + "/platform_group", delete_platform_group_by_id)

    app.router.add_post(prefix + "/platform", create_platform)
    app.router.add_put(prefix + "/platform", update_platform)
    app.router.add_delete(prefix + "/platform", delete_platform_by_id)
