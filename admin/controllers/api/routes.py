import aiohttp.web

import admin.controllers.api.shop.routes as shop_routes
from admin.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: str) -> None:
    include_urls(app, prefix + "/shop", shop_routes)
