from typing import Optional

import aiohttp.web

from admin.controllers.api import routes as api_urls
from admin.controllers.templates import routes as template_urls
from admin.utils.urls import include_urls


def init_routes(app: aiohttp.web.Application,
                prefix: Optional[str] = None) -> None:
    include_urls(app, "/api", api_urls)
    include_urls(app, None, template_urls)
