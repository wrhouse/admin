from typing import List, Callable

import aiohttp
import aiohttp.web
import jwt


SERVICE_SECRET = '25WxYfcNX6BxoUK2nSo6r8L6s9mLLgRv'
ALLOWED_USER_IDS = {1, 2}


def parse_jwt_token(jwt_token: str,
                    secret: str,
                    verify: bool = True) -> dict:
    jwt_data = jwt.decode(jwt_token,
                          secret,
                          leeway=10,
                          verify=verify,
                          algorithms=['HS256'])
    del jwt_data['created_at']
    del jwt_data['exp']
    return jwt_data


async def user_by_session_id(session_id: str) -> dict:
    async with aiohttp.ClientSession() as session:
        async with session.get(f"http://tokenizer_app:80/api/session/{session_id}") as resp:
            resp_json = await resp.json()
            return resp_json.get("data", {})


@aiohttp.web.middleware
async def auth_middleware(request: aiohttp.web.Request,
                          handler: Callable):
    request['user'] = dict(user_id=1,
                           name='Admin',
                           surname='GGWH')
    request['groups'] = [
        dict(name='Администратор'),
    ]
    return await handler(request)

    access_token = request.cookies.get("access_token")
    if access_token is not None:
        session_id = parse_jwt_token(access_token, SERVICE_SECRET)['session_id']
        user = await user_by_session_id(session_id)
        if user.get('user_id') in ALLOWED_USER_IDS:
            request['user'] = dict(user_id=user['user_id'],
                                   name=user['name'],
                                   surname=user['surname'])
            request['groups'] = [
                dict(name='Администратор'),
            ]
            return await handler(request)
    raise aiohttp.web.HTTPForbidden()


SERVICE_MIDDLEWARES = [auth_middleware]
