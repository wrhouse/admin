import os
from typing import Tuple

from pydantic import BaseSettings


class Settings(BaseSettings):
    @property
    def service_host(self) -> str:
        return os.environ["SERVICE_HOST"]

    @property
    def service_port(self) -> int:
        return int(os.environ["SERVICE_PORT"])

    @property
    def environment(self) -> str:
        return os.environ["ENVIRONMENT"]

    @property
    def database_host(self) -> str:
        return os.environ["DATABASE_HOST"]

    @property
    def database_port(self) -> int:
        return int(os.environ["DATABASE_PORT"])

    @property
    def database_user(self) -> str:
        return os.environ["DATABASE_USER"]

    @property
    def database_password(self) -> str:
        return os.environ["DATABASE_PASSWORD"]

    @property
    def database_name(self) -> str:
        return os.environ["DATABASE_NAME"]

    @property
    def shop_database_host(self) -> str:
        return os.environ["SHOP_DATABASE_HOST"]

    @property
    def shop_database_port(self) -> int:
        return int(os.environ["SHOP_DATABASE_PORT"])

    @property
    def shop_database_user(self) -> str:
        return os.environ["SHOP_DATABASE_USER"]

    @property
    def shop_database_password(self) -> str:
        return os.environ["SHOP_DATABASE_PASSWORD"]

    @property
    def shop_database_name(self) -> str:
        return os.environ["SHOP_DATABASE_NAME"]

    @property
    def path_to_templates(self) -> str:
        return os.environ["PATH_TO_TEMPLATES"]

    @property
    def path_to_static_files(self) -> str:
        return os.environ["PATH_TO_STATIC_FILES"]

    @property
    def aws_access_key_id(self) -> str:
        return os.environ['AWS_ACCESS_KEY_ID']

    @property
    def aws_secret_access_key(self) -> str:
        return os.environ['AWS_SECRET_ACCESS_KEY']

    @property
    def aws_bucket(self) -> str:
        return os.environ['AWS_BUCKET']

    @property
    def game_image_default_size(self) -> Tuple[int, int]:
        (width, height) = os.environ['GAME_IMAGE_DEFAULT_SIZE'].split('*')
        return int(width), int(height)

    @property
    def game_image_default_directory(self) -> str:
        return os.environ["GAME_IMAGE_DEFAULT_DIRECTORY"]

    @property
    def game_image_chat_size(self) -> Tuple[int, int]:
        (width, height) = os.environ['GAME_IMAGE_CHAT_SIZE'].split('*')
        return int(width), int(height)

    @property
    def game_image_chat_directory(self) -> str:
        return os.environ["GAME_IMAGE_CHAT_DIRECTORY"]

    @property
    def shop_image_default_size(self) -> Tuple[int, int]:
        (width, height) = os.environ["SHOP_IMAGE_DEFAULT_SIZE"].split('*')
        return int(width), int(height)

    @property
    def shop_image_default_directory(self) -> str:
        return os.environ["SHOP_IMAGE_DEFAULT_DIRECTORY"]

    @property
    def service_icons_default_directory(self) -> str:
        return os.environ["SERVICE_ICONS_DEFAULT_DIRECTORY"]
