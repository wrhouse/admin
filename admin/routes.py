import aiohttp.web

from admin.controllers import routes as urls
from admin.settings import Settings
from admin.utils.urls import include_urls, include_static_path


def init_routes(app: aiohttp.web.Application) -> None:
    include_urls(app, None, urls)

    settings = Settings()
    include_static_path(app, settings.path_to_static_files, '/assets')
    include_static_path(app, settings.path_to_static_files, '/css')
    include_static_path(app, settings.path_to_static_files, '/js')
    include_static_path(app, settings.path_to_static_files, '/plugins')
