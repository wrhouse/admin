from typing import Optional, List

from sqlalchemy.sql import and_

from admin.db.services.shop.models import ServiceInsurance


async def fn_get_service_insurance(db_connection,
                                   *,
                                   service_id: Optional[int]) -> List[ServiceInsurance]:
    select_query = ServiceInsurance.__table__.select()
    if service_id is not None:
        select_query = select_query.where(ServiceInsurance.service_id == service_id)
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def fn_create_service_insurance(db_connection,
                                      *,
                                      service_id: int,
                                      insurance_days: int) -> int:
    insert_query = (ServiceInsurance.__table__
                    .insert()
                    .values(service_id=service_id,
                            insurance_days=insurance_days))
    return await db_connection.scalar(insert_query)


async def fn_delete_service_insurance(db_connection,
                                      *,
                                      service_id: int,
                                      insurance_days: int) -> None:
    delete_query = (ServiceInsurance.__table__
                    .delete()
                    .where(and_(ServiceInsurance.service_id == service_id,
                                ServiceInsurance.insurance_days == insurance_days)))
    await db_connection.execute(delete_query)
