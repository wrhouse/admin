from typing import List

from admin.db.services.shop.models import GameOptionValue


async def fn_get_game_option_value(db_connection,
                                   *,
                                   game_option_id: int) -> List[GameOptionValue]:
    select_query = (GameOptionValue.__table__
                    .select()
                    .where(GameOptionValue.game_option_id == game_option_id))
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def fn_create_game_option_value(db_connection,
                                      *,
                                      game_option_id: int,
                                      game_option_value: str) -> int:
    insert_query = (GameOptionValue.__table__
                    .insert()
                    .values(game_option_id=game_option_id,
                            game_option_value=game_option_value))
    return await db_connection.scalar(insert_query)


async def fn_delete_game_option_value(db_connection,
                                      *,
                                      game_option_value_id: int) -> None:
    delete_query = (GameOptionValue.__table__
                    .delete()
                    .where(GameOptionValue.id == game_option_value_id))
    await db_connection.execute(delete_query)
