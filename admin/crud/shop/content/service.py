import hashlib
from base64 import b64decode
from collections import namedtuple
from io import BytesIO
from typing import List, Optional

from sqlalchemy import join

from admin.db.services.shop.models import Service, RelServiceGame
from admin.utils.images import upload_to_s3
from .service_duration import fn_get_service_duration, fn_create_service_duration, fn_delete_service_duration
from .service_insurance import fn_get_service_insurance, fn_create_service_insurance, fn_delete_service_insurance
from admin.settings import Settings


ExtendedService = namedtuple('ExtendedService', [
    'service',
    'full_name',
    'duration_days',
    'insurance_days'
])
ExtendedService.__new__.__defaults__ = (None,) * len(ExtendedService._fields)


settings = Settings()


async def fn_get_service(db_connection,
                         *,
                         service_id: Optional[int] = None,
                         parent_id: Optional[int] = None,
                         game_id: Optional[int] = None,
                         extended: bool = False,
                         with_parent_prefix: bool = False) -> List[ExtendedService]:
    select_query = Service.__table__.select()

    if game_id is not None:
        join_subquery = join(Service,
                             RelServiceGame,
                             Service.id == RelServiceGame.service_id)
        select_query = (select_query
                        .select_from(join_subquery)
                        .where(RelServiceGame.game_id == game_id))

    if service_id is not None:
        select_query = select_query.where(Service.id == service_id)

    if parent_id is not None:
        select_query = select_query.where(Service.parent_id == parent_id)

    cursor = await db_connection.execute(select_query)
    services = await cursor.fetchall()

    service_names = {}
    if with_parent_prefix:
        service_id_name_mapper = {service.id: service.name for service in services}
        service_id_parent_mapper = {service.id: service.parent_id for service in services}
        for index in range(len(services)):
            current_parent_id = services[index].parent_id
            current_service_name = services[index].name
            while current_parent_id:
                current_service_name = service_id_name_mapper[current_parent_id] + ' → ' + current_service_name
                current_parent_id = service_id_parent_mapper[current_parent_id]
            service_names[services[index].id] = current_service_name

    if extended:
        services = [ExtendedService(service=service,
                                    full_name=service_names.get(service.id, service.name),
                                    insurance_days=await fn_get_service_insurance(db_connection, service_id=service.id),
                                    duration_days=await fn_get_service_duration(db_connection, service_id=service.id))
                    for service in services]
    else:
        services = [ExtendedService(service=service,
                                    full_name=service_names.get(service.id, service.name))
                    for service in services]

    return services


async def fn_create_service(db_connection,
                            *,
                            service_name: str,
                            parent_id: Optional[int],
                            icon: Optional[str],
                            delivery_guidelines: str,
                            duration_days: Optional[List[int]] = None,
                            insurance_days: Optional[List[int]] = None) -> int:
    if duration_days is None:
        duration_days = []
    if insurance_days is None:
        insurance_days = []

    insert_values = dict(name=service_name,
                         parent_id=parent_id,
                         delivery_guidelines=delivery_guidelines)

    if icon and icon.startswith('data:'):
        icon_filename = hashlib.md5(icon.encode('utf-8')).hexdigest()
        icon_url = await upload_to_s3(filename=f'{icon_filename}.svg',
                                      data=BytesIO(b64decode(icon.split(',')[1])),
                                      directory=settings.service_icons_default_directory,
                                      content_type='image/svg+xml')
        if icon_url is not None:
            insert_values['icon_url'] = icon_url

    insert_query = (Service.__table__
                    .insert()
                    .values(**insert_values))
    service_id = await db_connection.scalar(insert_query)

    for duration_days_value in duration_days:
        await fn_create_service_duration(db_connection,
                                         service_id=service_id,
                                         duration_days=duration_days_value)

    for insurance_days_value in insurance_days:
        await fn_create_service_insurance(db_connection,
                                          service_id=service_id,
                                          insurance_days=insurance_days_value)

    return service_id


async def fn_update_service(db_connection,
                            *,
                            service_id: int,
                            parent_id: Optional[int],
                            service_name: str,
                            icon: Optional[str],
                            delivery_guidelines: str,
                            duration_days: Optional[List[int]] = None,
                            insurance_days: Optional[List[int]] = None) -> None:
    if duration_days is None:
        duration_days = []
    if insurance_days is None:
        insurance_days = []

    update_values = dict(name=service_name,
                         parent_id=parent_id,
                         delivery_guidelines=delivery_guidelines)

    if icon and icon.startswith('data:'):
        icon_filename = hashlib.md5(icon.encode('utf-8')).hexdigest()
        icon_url = await upload_to_s3(filename=f'{icon_filename}.svg',
                                      data=BytesIO(b64decode(icon.split(',')[1])),
                                      directory=settings.service_icons_default_directory,
                                      content_type='image/svg+xml')
        if icon_url is not None:
            update_values['icon_url'] = icon_url

    update_query = (Service.__table__
                    .update()
                    .where(Service.id == service_id)
                    .values(**update_values))
    await db_connection.execute(update_query)

    service_duration_days = await fn_get_service_duration(db_connection,
                                                          service_id=service_id)
    old_duration_days = set(map(lambda item: item.duration_days, service_duration_days))
    new_duration_days = set(duration_days)

    service_insurance_days = await fn_get_service_insurance(db_connection,
                                                            service_id=service_id)
    old_insurance_days = set(map(lambda item: item.insurance_days, service_insurance_days))
    new_insurance_days = set(insurance_days)

    for d_days in (new_duration_days - old_duration_days):
        await fn_create_service_duration(db_connection,
                                         service_id=service_id,
                                         duration_days=d_days)
    for d_days in (old_duration_days - new_duration_days):
        await fn_delete_service_duration(db_connection,
                                         service_id=service_id,
                                         duration_days=d_days)

    for i_days in (new_insurance_days - old_insurance_days):
        await fn_create_service_insurance(db_connection,
                                          service_id=service_id,
                                          insurance_days=i_days)
    for i_days in (old_insurance_days - new_insurance_days):
        await fn_delete_service_insurance(db_connection,
                                          service_id=service_id,
                                          insurance_days=i_days)


async def fn_delete_service(db_connection,
                            *,
                            service_id: int) -> None:
    delete_query = (Service.__table__
                    .delete()
                    .where(Service.id == service_id))
    await db_connection.execute(delete_query)
