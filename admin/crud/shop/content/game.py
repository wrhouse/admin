from typing import List, Optional

from sqlalchemy.sql import and_

from admin.crud.shop.content.game_option import fn_create_game_option, fn_get_game_option, fn_delete_game_option
from admin.crud.shop.content.game_option_value import fn_create_game_option_value
from admin.crud.shop.content.platform import fn_get_platform
from admin.crud.shop.content.service import fn_get_service
from admin.db.services.shop.models import (
    Game,
    RelGamePlatform,
    RelServiceGame,
    RelDeliveryOptionGameService,
    GameOption,
    GameOptionValue)
from admin.settings import Settings
from admin.utils import url_encode
from admin.utils.images import create_and_upload_image


settings = Settings()


async def fn_get_game(db_connection,
                      *,
                      game_id: Optional[int] = None,
                      extended: bool = False) -> List[dict]:
    games_result = []

    select_games_query = Game.__table__.select()
    if game_id is not None:
        select_games_query = select_games_query.where(Game.id == game_id)
    games_cursor = await db_connection.execute(select_games_query)

    for game in await games_cursor.fetchall():
        game_platform_ids = []
        game_services = []
        if extended:
            # Получение идентификаторов платформ для текущей игры
            select_platforms_query = (RelGamePlatform.__table__
                                      .select()
                                      .where(RelGamePlatform.game_id == game.id))
            platforms_cursor = await db_connection.execute(select_platforms_query)
            for platform in await platforms_cursor.fetchall():
                game_platform_ids.append(platform.platform_id)

            # Получение информации об услугах для текущей игры
            select_services_query = (RelServiceGame.__table__
                                     .select()
                                     .where(RelServiceGame.game_id == game.id))
            services_cursor = await db_connection.execute(select_services_query)
            for service in await services_cursor.fetchall():
                delivery_option_ids = []
                game_options = []

                # Получение идентификаторов опций доставки для текущей игры и услуги
                select_delivery_options_query = (RelDeliveryOptionGameService.__table__
                                                 .select()
                                                 .where(and_(RelDeliveryOptionGameService.service_id == service.service_id,
                                                             RelDeliveryOptionGameService.game_id == game.id)))
                delivery_options_cursor = await db_connection.execute(select_delivery_options_query)
                for delivery_option in await delivery_options_cursor.fetchall():
                    delivery_option_ids.append(delivery_option.delivery_option_id)

                # Получение опций для текущей игры и услуги
                select_game_options_query = (GameOption.__table__
                                             .select()
                                             .where(and_(GameOption.service_id == service.service_id,
                                                         GameOption.game_id == game.id))
                                             .order_by(GameOption.position.asc()))
                game_options_cursor = await db_connection.execute(select_game_options_query)
                for game_option in await game_options_cursor.fetchall():
                    game_option_values = []

                    select_game_option_values_query = (GameOptionValue.__table__
                                                       .select()
                                                       .where(GameOptionValue.game_option_id == game_option.id))
                    game_option_values_cursor = await db_connection.execute(select_game_option_values_query)
                    for game_option_value in await game_option_values_cursor.fetchall():
                        game_option_values.append({
                            'game_option_value_id': game_option_value.id,
                            'game_option_value': game_option_value.game_option_value
                        })

                    game_options.append({
                        'game_option_id': game_option.id,
                        'game_option_name': game_option.game_option_name,
                        'game_option_position': game_option.position,
                        'game_option_values': game_option_values
                    })

                game_services.append({
                    'service_id': service.service_id,
                    'price_unit_placeholder': service.price_unit_placeholder,
                    'advanced_form_name': service.advanced_form_name,
                    'delivery_option_ids': delivery_option_ids,
                    'game_options': game_options
                })

        games_result.append({
            'game_id': game.id,
            'game_name': game.name,
            'game_description': game.description,
            'game_url': game.url,
            'game_image_url': game.image_url,
            'game_chat_image_url': game.chat_image_url,
            'game_platform_ids': game_platform_ids,
            'game_services': game_services
        })

    return games_result


async def fn_create_game(db_connection,
                         *,
                         game_name: str,
                         game_description: str,
                         game_image: Optional[str],
                         game_chat_image: Optional[str],
                         game_platform_ids: Optional[List[int]] = None,
                         game_services: Optional[List[dict]] = None) -> int:
    if game_platform_ids is None:
        game_platform_ids = []

    if game_services is None:
        game_services = []

    insert_values = dict(name=game_name,
                         description=game_description,
                         url=url_encode(game_name))

    if game_image is not None:
        insert_values['image_url'] = await create_and_upload_image(base64_image=game_image,
                                                                   size=settings.game_image_default_size,
                                                                   directory=settings.game_image_default_directory)
    if game_chat_image is not None:
        insert_values['chat_image_url'] = await create_and_upload_image(base64_image=game_chat_image,
                                                                        size=settings.game_image_chat_size,
                                                                        directory=settings.game_image_chat_directory)

    insert_query = Game.__table__.insert().values(**insert_values)
    game_id = await db_connection.scalar(insert_query)

    for platform_id in game_platform_ids:
        insert_query = (RelGamePlatform.__table__
                        .insert()
                        .values(game_id=game_id,
                                platform_id=platform_id))
        await db_connection.execute(insert_query)

    for service_info in game_services:
        service_id = service_info['service_id']
        game_options = service_info['game_options']
        delivery_option_ids = service_info['delivery_option_ids']
        price_unit_placeholder = service_info['price_unit_placeholder']
        advanced_form_name = service_info['advanced_form_name']

        insert_query = (RelServiceGame.__table__
                        .insert()
                        .values(game_id=game_id,
                                service_id=service_id,
                                price_unit_placeholder=price_unit_placeholder,
                                advanced_form_name=advanced_form_name))
        await db_connection.execute(insert_query)

        for delivery_option_id in delivery_option_ids:
            insert_query = (RelDeliveryOptionGameService.__table__
                            .insert()
                            .values(delivery_option_id=delivery_option_id,
                                    game_id=game_id,
                                    service_id=service_id))
            await db_connection.execute(insert_query)

        for game_option in game_options:
            game_option_name = game_option['game_option_name']
            game_option_position = game_option.get('game_option_position') or 0
            if game_option_name:
                game_option_values = game_option['game_option_values']

                game_option_is_empty = all(not option_value for option_value in game_option_values)
                if game_option_is_empty:
                    continue

                game_option_id = await fn_create_game_option(db_connection,
                                                             game_id=game_id,
                                                             service_id=service_id,
                                                             game_option_name=game_option_name,
                                                             position=game_option_position)
                for option_value in game_option_values:
                    if option_value:
                        game_option_value = option_value['game_option_value']
                        await fn_create_game_option_value(db_connection,
                                                          game_option_id=game_option_id,
                                                          game_option_value=game_option_value)
    return game_id


async def fn_update_game(db_connection,
                         *,
                         game_id: int,
                         game_name: str,
                         game_description: str,
                         game_image: Optional[str],
                         game_chat_image: Optional[str],
                         game_platform_ids: Optional[List[int]] = None,
                         game_services: Optional[List[dict]] = None) -> None:
    if game_platform_ids is None:
        game_platform_ids = []
    
    if game_services is None:
        game_services = []

    update_values = dict(name=game_name,
                         description=game_description,
                         url=url_encode(game_name))

    if game_image is not None and game_image.startswith('data:'):
        update_values['image_url'] = await create_and_upload_image(base64_image=game_image,
                                                                   size=settings.game_image_default_size,
                                                                   directory=settings.game_image_default_directory)
    if game_chat_image is not None and game_chat_image.startswith('data:'):
        update_values['chat_image_url'] = await create_and_upload_image(base64_image=game_chat_image,
                                                                        size=settings.game_image_chat_size,
                                                                        directory=settings.game_image_chat_directory)

    update_game_query = (Game.__table__
                         .update()
                         .where(Game.id == game_id)
                         .values(**update_values))
    await db_connection.execute(update_game_query)

    new_game_platform_ids = set(game_platform_ids)
    old_game_platform_ids = set(extended_platform.platform.id
                                for extended_platform in await fn_get_platform(db_connection,
                                                                               game_id=game_id))
    for added_platform_id in (new_game_platform_ids - old_game_platform_ids):
        insert_game_platform_query = (RelGamePlatform.__table__
                                      .insert()
                                      .values(game_id=game_id,
                                              platform_id=added_platform_id))
        await db_connection.execute(insert_game_platform_query)
    for deleted_platform_id in (old_game_platform_ids - new_game_platform_ids):
        delete_game_platform_query = (RelGamePlatform.__table__
                                      .delete()
                                      .where(and_(RelGamePlatform.game_id == game_id,
                                                  RelGamePlatform.platform_id == deleted_platform_id)))
        await db_connection.execute(delete_game_platform_query)

    new_game_service_ids = set(service['service_id'] for service in game_services)
    old_game_service_ids = set(extended_service.service.id
                               for extended_service in await fn_get_service(db_connection,
                                                                            game_id=game_id))

    for deleted_game_service_id in (old_game_service_ids - new_game_service_ids):
        delete_game_service_query = (RelServiceGame.__table__
                                     .delete()
                                     .where(and_(RelServiceGame.service_id == deleted_game_service_id,
                                                 RelServiceGame.game_id == game_id)))
        await db_connection.execute(delete_game_service_query)

    added_game_service_ids = new_game_service_ids - old_game_service_ids
    for service in game_services:
        service_id = service['service_id']
        game_options = service['game_options']
        delivery_option_ids = service['delivery_option_ids']
        price_unit_placeholder = service['price_unit_placeholder']
        advanced_form_name = service['advanced_form_name']

        if service_id in added_game_service_ids:
            insert_game_service_query = (RelServiceGame.__table__
                                         .insert()
                                         .values(game_id=game_id,
                                                 service_id=service_id,
                                                 price_unit_placeholder=price_unit_placeholder,
                                                 advanced_form_name=advanced_form_name))
            await db_connection.execute(insert_game_service_query)

            for delivery_option_id in delivery_option_ids:
                insert_query = (RelDeliveryOptionGameService.__table__
                                .insert()
                                .values(delivery_option_id=delivery_option_id,
                                        game_id=game_id,
                                        service_id=service_id))
                await db_connection.execute(insert_query)

            for game_option in game_options:
                game_option_name = game_option['game_option_name']
                game_option_position = game_option.get('game_option_position') or 0
                if game_option_name:
                    game_option_values = game_option['game_option_values']

                    game_option_is_empty = all(not option_value for option_value in game_option_values)
                    if game_option_is_empty:
                        continue

                    game_option_id = await fn_create_game_option(db_connection,
                                                                 game_id=game_id,
                                                                 service_id=service_id,
                                                                 game_option_name=game_option_name,
                                                                 position=game_option_position)
                    for option_value in game_option_values:
                        if option_value:
                            game_option_value = option_value['game_option_value']
                            await fn_create_game_option_value(db_connection,
                                                              game_option_id=game_option_id,
                                                              game_option_value=game_option_value)
        else:
            update_game_service_query = (RelServiceGame.__table__
                                         .update()
                                         .where(and_(RelServiceGame.service_id == service_id,
                                                     RelServiceGame.game_id == game_id))
                                         .values(price_unit_placeholder=price_unit_placeholder,
                                                 advanced_form_name=advanced_form_name))
            await db_connection.execute(update_game_service_query)

            new_delivery_option_ids = set(delivery_option_ids)
            select_delivery_option_query = (RelDeliveryOptionGameService.__table__
                                            .select()
                                            .where(and_(RelDeliveryOptionGameService.service_id == service_id,
                                                        RelDeliveryOptionGameService.game_id == game_id)))
            delivery_option_cursor = await db_connection.execute(select_delivery_option_query)
            old_delivery_option_ids = set(delivery_option.delivery_option_id
                                          for delivery_option in await delivery_option_cursor.fetchall())

            for added_delivery_option_id in (new_delivery_option_ids - old_delivery_option_ids):
                insert_delivery_option_query = (RelDeliveryOptionGameService.__table__
                                                .insert()
                                                .values(delivery_option_id=added_delivery_option_id,
                                                        game_id=game_id, service_id=service_id))
                await db_connection.execute(insert_delivery_option_query)
            for deleted_delivery_option_id in (old_delivery_option_ids - new_delivery_option_ids):
                delete_delivery_option_query = (RelDeliveryOptionGameService.__table__
                                                .delete()
                                                .where(and_(RelDeliveryOptionGameService.delivery_option_id == deleted_delivery_option_id,
                                                            RelDeliveryOptionGameService.game_id == game_id,
                                                            RelDeliveryOptionGameService.service_id == service_id)))
                await db_connection.execute(delete_delivery_option_query)

            new_game_option_ids = set(game_option['game_option_id']
                                      for game_option in game_options
                                      if game_option['game_option_id'] is not None)
            old_game_option_ids = set(extended_game_option.game_option.id
                                      for extended_game_option in await fn_get_game_option(db_connection,
                                                                                           game_id=game_id,
                                                                                           service_id=service_id))

            for deleted_game_option_id in (old_game_option_ids - new_game_option_ids):
                await fn_delete_game_option(db_connection, game_option_id=deleted_game_option_id)

            for game_option in game_options:
                game_option_id = game_option['game_option_id']
                game_option_name = game_option['game_option_name']
                game_option_position = game_option.get('game_option_position') or 0
                game_option_values = game_option['game_option_values']

                if not game_option_name:
                    continue

                if game_option_id is None:
                    game_option_id = await fn_create_game_option(db_connection,
                                                                 game_id=game_id,
                                                                 service_id=service_id,
                                                                 game_option_name=game_option_name,
                                                                 position=game_option_position)

                    game_option_is_empty = all(not option_value for option_value in game_option_values)
                    if game_option_is_empty:
                        continue

                    for option_value in game_option_values:
                        if option_value:
                            game_option_value = option_value['game_option_value']
                            await fn_create_game_option_value(db_connection,
                                                              game_option_id=game_option_id,
                                                              game_option_value=game_option_value)
                else:
                    update_game_option_query = (GameOption.__table__
                                                .update()
                                                .where(GameOption.id == game_option_id)
                                                .values(game_option_name=game_option_name,
                                                        position=game_option_position))
                    await db_connection.execute(update_game_option_query)

                    new_game_option_value_ids = set(game_option_value['game_option_value_id']
                                                    for game_option_value in game_option_values
                                                    if game_option_value['game_option_value_id'] is not None)
                    select_game_option_values_query = (GameOptionValue.__table__
                                                       .select()
                                                       .where(GameOptionValue.game_option_id == game_option_id))
                    game_option_values_cursor = await db_connection.execute(select_game_option_values_query)
                    old_game_option_value_ids = set(game_option_value.id
                                                    for game_option_value in await game_option_values_cursor.fetchall())

                    for deleted_game_option_value_id in (old_game_option_value_ids - new_game_option_value_ids):
                        delete_game_option_value_query = (GameOptionValue.__table__
                                                          .delete()
                                                          .where(GameOptionValue.id == deleted_game_option_value_id))
                        await db_connection.execute(delete_game_option_value_query)

                    for option_value in game_option_values:
                        game_option_value_id = option_value['game_option_value_id']
                        game_option_value = option_value['game_option_value']

                        if game_option_value_id is None:
                            insert_game_option_value_query = (GameOptionValue.__table__
                                                              .insert()
                                                              .values(game_option_id=game_option_id,
                                                                      game_option_value=game_option_value))
                            await db_connection.execute(insert_game_option_value_query)
                        else:
                            update_game_option_value_query = (GameOptionValue.__table__
                                                              .update()
                                                              .where(GameOptionValue.id == game_option_value_id)
                                                              .values(game_option_value=game_option_value))
                            await db_connection.execute(update_game_option_value_query)


async def fn_delete_game(db_connection,
                         *,
                         game_id: int) -> None:
    delete_query = (Game.__table__
                    .delete()
                    .where(Game.id == game_id))
    await db_connection.execute(delete_query)
