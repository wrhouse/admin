from admin.db.services.shop.models import ProductDeliveryOption


async def fn_delete_product_delivery_option(db_connection,
                                            *,
                                            product_delivery_option_id: int) -> None:
    delete_query = (ProductDeliveryOption.__table__
                    .delete()
                    .where(ProductDeliveryOption.id == product_delivery_option_id))
    await db_connection.execute(delete_query)
