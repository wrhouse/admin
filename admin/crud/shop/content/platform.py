from collections import namedtuple
from typing import List, Optional

from sqlalchemy import join, select

from admin.db.services.shop.models import Platform, PlatformGroup, RelGamePlatform


ExtendedPlatform = namedtuple('ExtendedPlatform', [
    'platform',
    'platform_group'
])
ExtendedPlatform.__new__.__defaults__ = (None,) * len(ExtendedPlatform._fields)


def joined_item_to_extended_platform(item) -> ExtendedPlatform:
    return ExtendedPlatform(platform=Platform(id=item.platform_id,
                                              group_id=item.platform_group_id,
                                              name=item.platform_name),
                            platform_group=PlatformGroup(id=item.platform_group_id,
                                                         name=item.platform_group_name))


async def fn_get_platform(db_connection,
                          *,
                          platform_id: Optional[int] = None,
                          game_id: Optional[int] = None,
                          extended: bool = False) -> List[ExtendedPlatform]:
    if extended:
        join_subquery = join(Platform,
                             PlatformGroup,
                             Platform.group_id == PlatformGroup.id,
                             isouter=True)
        select_query = (select([Platform.id,
                                Platform.name,
                                PlatformGroup.id,
                                PlatformGroup.name])
                        .select_from(join_subquery))
    else:
        select_query = Platform.__table__.select()

    if game_id is not None:
        join_subquery = join(Platform,
                             RelGamePlatform,
                             Platform.id == RelGamePlatform.platform_id)
        select_query = (select_query
                        .select_from(join_subquery)
                        .where(RelGamePlatform.game_id == game_id))

    if platform_id is not None:
        select_query = select_query.where(Platform.id == platform_id)

    cursor = await db_connection.execute(select_query)
    platforms = await cursor.fetchall()

    if extended:
        platforms = [joined_item_to_extended_platform(platform) for platform in platforms]
    else:
        platforms = [ExtendedPlatform(platform=platform) for platform in platforms]

    return platforms


async def fn_create_platform(db_connection,
                             *,
                             platform_group_id: int,
                             platform_name: str) -> int:
    insert_query = (Platform.__table__
                    .insert()
                    .values(group_id=platform_group_id,
                            name=platform_name))
    return await db_connection.scalar(insert_query)


async def fn_update_platform(db_connection,
                             *,
                             platform_id: int,
                             platform_group_id: int,
                             platform_name: str) -> None:
    update_query = (Platform.__table__
                    .update()
                    .where(Platform.id == platform_id)
                    .values(group_id=platform_group_id,
                            name=platform_name))
    await db_connection.execute(update_query)


async def fn_delete_platform(db_connection,
                             *,
                             platform_id: int) -> None:
    delete_query = (Platform.__table__
                    .delete()
                    .where(Platform.id == platform_id))
    await db_connection.execute(delete_query)
