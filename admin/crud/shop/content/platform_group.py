from typing import List, Optional

from admin.db.services.shop.models import PlatformGroup


async def fn_get_platform_group(db_connection,
                                *,
                                platform_group_id: Optional[int] = None) -> List[PlatformGroup]:
    select_query = PlatformGroup.__table__.select()

    if platform_group_id is not None:
        select_query = select_query.where(PlatformGroup.id == platform_group_id)

    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def fn_create_platform_group(db_connection,
                                   *,
                                   platform_group_name: str) -> int:
    insert_query = (PlatformGroup.__table__
                    .insert()
                    .values(name=platform_group_name))
    return await db_connection.scalar(insert_query)


async def fn_update_platform_group(db_connection,
                                   *,
                                   platform_group_id: int,
                                   platform_group_name: str) -> None:
    update_query = (PlatformGroup.__table__
                    .update()
                    .where(PlatformGroup.id == platform_group_id)
                    .values(name=platform_group_name))
    await db_connection.execute(update_query)


async def fn_delete_platform_group(db_connection,
                                   *,
                                   platform_group_id: int) -> None:
    delete_query = (PlatformGroup.__table__
                    .delete()
                    .where(PlatformGroup.id == platform_group_id))
    await db_connection.execute(delete_query)
