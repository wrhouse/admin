from typing import Optional, List

from admin.db.services.shop.models import Shop
from admin.settings import Settings
from admin.utils import url_encode


settings = Settings()


async def fn_get_shop(db_connection,
                      *,
                      shop_id: Optional[int] = None,
                      owner_id: Optional[int] = None,
                      url: Optional[str] = None) -> List[Shop]:
    select_query = Shop.__table__.select()

    if shop_id is not None:
        select_query = select_query.where(Shop.id == shop_id)

    if owner_id is not None:
        select_query = select_query.where(Shop.owner_id == owner_id)

    if url is not None:
        select_query = select_query.where(Shop.url == url)

    cursor = await db_connection.execute(select_query)
    return cursor.fetchall()


async def fn_create_shop(db_connection,
                         *,
                         owner_id: int,
                         name: str) -> int:
    insert_query = (Shop.__table__
                    .insert()
                    .values(owner_id=owner_id,
                            name=name,
                            url=url_encode(name)))
    return await db_connection.scalar(insert_query)


async def fn_update_shop(db_connection,
                         *,
                         shop_id: int,
                         owner_id: Optional[int] = None,
                         name: Optional[str] = None) -> None:
    update_values = dict()
    if owner_id is not None:
        update_values['owner_id'] = owner_id
    if name is not None:
        update_values['name'] = name
        update_values['url'] = url_encode(name)

    update_query = (Shop.__table__
                    .update()
                    .where(Shop.id == shop_id)
                    .values(**update_values))
    await db_connection.execute(update_query)


async def fn_delete_shop(db_connection,
                         *,
                         shop_id: int) -> None:
    delete_query = Shop.__table__.delete().where(Shop.id == shop_id)
    await db_connection.execute(delete_query)
