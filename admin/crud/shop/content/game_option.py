from collections import namedtuple
from typing import List

from sqlalchemy.sql import and_

from admin.db.services.shop.models import GameOption
from .game_option_value import fn_get_game_option_value


ExtendedGameOption = namedtuple('ExtendedGameOption', [
    'game_option',
    'game_option_values'
])
ExtendedGameOption.__new__.__defaults__ = (None,) * len(ExtendedGameOption._fields)


async def fn_get_game_option(db_connection,
                             *,
                             game_id: int,
                             service_id: int,
                             extended: bool = False) -> List[ExtendedGameOption]:
    select_query = (GameOption.__table__
                    .select()
                    .where(and_(GameOption.game_id == game_id,
                                GameOption.service_id == service_id)))
    cursor = await db_connection.execute(select_query)
    game_options = await cursor.fetchall()

    if extended:
        game_options = [ExtendedGameOption(game_option=game_option,
                                           game_option_values=await fn_get_game_option_value(db_connection, game_option_id=game_option.id))
                        for game_option in game_options]
    else:
        game_options = [ExtendedGameOption(game_option=game_option) for game_option in game_options]

    return game_options


async def fn_create_game_option(db_connection,
                                *,
                                game_id: int,
                                service_id: int,
                                game_option_name: str,
                                position: int) -> int:
    insert_query = (GameOption.__table__
                    .insert()
                    .values(game_id=game_id,
                            service_id=service_id,
                            game_option_name=game_option_name,
                            position=position))
    return await db_connection.scalar(insert_query)


async def fn_delete_game_option(db_connection,
                                *,
                                game_option_id: int) -> None:
    delete_query = (GameOption.__table__
                    .delete()
                    .where(GameOption.id == game_option_id))
    await db_connection.execute(delete_query)
