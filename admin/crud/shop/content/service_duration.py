from typing import Optional, List

from sqlalchemy.sql import and_

from admin.db.services.shop.models import ServiceDuration


async def fn_get_service_duration(db_connection,
                                  *,
                                  service_id: Optional[int]) -> List[ServiceDuration]:
    select_query = ServiceDuration.__table__.select()
    if service_id is not None:
        select_query = select_query.where(ServiceDuration.service_id == service_id)
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def fn_create_service_duration(db_connection,
                                     *,
                                     service_id: int,
                                     duration_days: int) -> int:
    insert_query = (ServiceDuration.__table__
                    .insert()
                    .values(service_id=service_id,
                            duration_days=duration_days))
    return await db_connection.scalar(insert_query)


async def fn_delete_service_duration(db_connection,
                                     *,
                                     service_id: int,
                                     duration_days: int) -> None:
    delete_query = (ServiceDuration.__table__
                    .delete()
                    .where(and_(ServiceDuration.service_id == service_id,
                                ServiceDuration.duration_days == duration_days)))
    await db_connection.execute(delete_query)
