from typing import List, Optional

from aiopg.sa import SAConnection
from sqlalchemy import join

from admin.db.services.shop.models import (
    DeliveryOption,
    RelDeliveryOptionGameService
)


async def fn_get_delivery_option(db_connection: SAConnection,
                                 *,
                                 delivery_option_id: Optional[int] = None,
                                 game_id: Optional[int] = None,
                                 service_id: Optional[int] = None) -> List[DeliveryOption]:
    select_query = DeliveryOption.__table__.select()

    if game_id is not None or service_id is not None:
        join_subquery = join(DeliveryOption,
                             RelDeliveryOptionGameService,
                             DeliveryOption.id == RelDeliveryOptionGameService.delivery_option_id)
        select_query = select_query.select_from(join_subquery)

        if game_id is not None:
            select_query = select_query.where(RelDeliveryOptionGameService.game_id == game_id)
        if service_id is not None:
            select_query = select_query.where(RelDeliveryOptionGameService.service_id == service_id)

    if delivery_option_id is not None:
        select_query = select_query.where(DeliveryOption.id == delivery_option_id)

    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def fn_create_delivery_option(db_connection: SAConnection,
                                    *,
                                    delivery_option_name: str) -> None:
    insert_query = (DeliveryOption.__table__
                    .insert()
                    .values(name=delivery_option_name))
    await db_connection.execute(insert_query)


async def fn_update_delivery_option(db_connection,
                                    *,
                                    delivery_option_id: int,
                                    delivery_option_name: str) -> int:
    update_query = (DeliveryOption.__table__
                    .update()
                    .where(DeliveryOption.id == delivery_option_id)
                    .values(name=delivery_option_name))
    return await db_connection.execute(update_query)


async def fn_delete_delivery_option(db_connection,
                                    *,
                                    delivery_option_id: int) -> None:
    delete_query = (DeliveryOption.__table__
                    .delete()
                    .where(DeliveryOption.id == delivery_option_id))
    await db_connection.execute(delete_query)
