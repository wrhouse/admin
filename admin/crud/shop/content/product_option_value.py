from admin.db.services.shop.models import ProductOptionValue


async def fn_delete_product_option_value(db_connection,
                                         *,
                                         product_option_value_id: int) -> None:
    delete_query = (ProductOptionValue.__table__
                    .delete()
                    .where(ProductOptionValue.id == product_option_value_id))
    await db_connection.execute(delete_query)
