from typing import List

from admin.db.services.shop.models import Language


async def fn_get_languages(db_connection) -> List[Language]:
    select_query = Language.__table__.select()
    cursor = await db_connection.execute(select_query)
    return await cursor.fetchall()


async def fn_add_language(db_connection,
                          *,
                          lang_code: str,
                          lang_name: str) -> None:
    insert_query = (Language.__table__
                    .insert()
                    .values(lang_code=lang_code,
                            lang_name=lang_name))
    await db_connection.execute(insert_query)


async def fn_delete_language(db_connection,
                             *,
                             lang_code: str) -> None:
    delete_query = (Language.__table__
                    .delete()
                    .where(Language.lang_code == lang_code))
    await db_connection.execute(delete_query)


async def fn_update_language(db_connection, *, lang_code: str, lang_name: str) -> None:
    update_query = (Language.__table__
                    .update()
                    .where(Language.lang_code == lang_code)
                    .values(lang_name=lang_name))
    await db_connection.execute(update_query)

