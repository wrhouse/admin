from typing import Dict

from sqlalchemy.sql import and_

from admin.crud.shop.localization import DEFAULT_LANG_CODE
import psycopg2

from admin.db.services.shop.models import Localization


async def fn_get_localizations(db_connection) -> Dict[str, Dict[str, str]]:
    select_query = Localization.__table__.select()
    cursor = await db_connection.execute(select_query)
    localizations = await cursor.fetchall()

    localization_dict = {}
    for localization in localizations:
        if localization.text_code not in localization_dict:
            localization_dict[localization.text_code] = {}
        localization[localization.text_code][localization.lang_code] = localization.text_value

    return localization_dict


async def fn_add_localization_text(db_connection,
                                   *,
                                   text_code: str,
                                   lang_code: str,
                                   text_value) -> None:
    insert_query = (Localization.__table__
                    .insert()
                    .values(text_code=text_code,
                            lang_code=lang_code,
                            text_value=text_value))
    await db_connection.execute(insert_query)


async def fn_add_localization(db_connection,
                              *,
                              text_code: str,
                              text_value: str) -> None:
    await fn_add_localization_text(db_connection,
                                   text_code=text_code,
                                   lang_code=DEFAULT_LANG_CODE,
                                   text_value=text_value)


async def fn_update_localization_text(db_connection,
                                      *,
                                      text_code: str,
                                      lang_code: str,
                                      text_value: str) -> None:
    update_query = (Localization.__table__
                    .update()
                    .where(and_(Localization.text_code == text_code,
                                Localization.lang_code == lang_code))
                    .values(text_value=text_value))
    await db_connection.execute(update_query)


async def fn_create_or_update_localization_text(db_connection,
                                                *,
                                                text_code: str,
                                                lang_code: str,
                                                text_value: str):
    # TODO: Fix inoup
    try:
        await fn_add_localization_text(db_connection,
                                       text_code=text_code,
                                       lang_code=lang_code,
                                       text_value=text_value)
    except psycopg2.errors.UniqueViolation:
        await fn_update_localization_text(db_connection,
                                          text_code=text_code,
                                          lang_code=lang_code,
                                          text_value=text_value)


async def fn_delete_localization_text(db_connection,
                                      *,
                                      text_code: str,
                                      lang_code: str) -> None:
    delete_query = (Localization.__table__
                    .delete()
                    .where(and_(Localization.text_code == text_code,
                                Localization.lang_code == lang_code)))
    await db_connection.execute(delete_query)


async def fn_delete_localization(db_connection,
                                 *,
                                 text_code: str) -> None:
    delete_query = (Localization.__table__
                    .delete()
                    .where(Localization.text_code == text_code))
    await db_connection.execute(delete_query)
