from typing import Dict, List, Callable

import aiohttp.web

from admin.settings import Settings
settings = Settings()

def append_filter(import_url: str, arr: List):
    arr.append(import_url)
    return ""


def setup_jinja2_filters() -> Dict[str, Callable]:
    return {
        "append": append_filter
    }


async def with_request_context(request: aiohttp.web.Request):
    return {'request': request}


async def imports_ctx(request: aiohttp.web.Request):
    return {'js_imports': []}


async def with_setting_ctx(request: aiohttp.web.Request):
    return {'settings': settings}


def setup_jinja2_context_processors() -> List[Callable]:
    return [
        with_request_context,
        imports_ctx,
        with_setting_ctx
    ]


__all__ = ['setup_jinja2_filters', 'setup_jinja2_context_processors']
